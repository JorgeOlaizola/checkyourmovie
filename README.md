# CheckYourMovie app

Application calls uses The Movie Database API (https://developers.themoviedb.org/) to fetch movies and tv shows. Developed as a final project for the Applaudo´s Trainee Program

## Try it

Link to deploy: https://checkyourmovie.vercel.app/
## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/JorgeOlaizola/checkyourmovie.git
```

Once the project it´s cloned, install dependencies

```bash
  npm install
```

Create a .env file at the root of the project and put the following credentials.

```js
  REACT_APP_BASE_URL=https://api.themoviedb.org/3
  REACT_APP_API_KEY= <<Your API_KEY>> // If you dont have one, you can get it by signing up in TMDB official website.
```


Execute the following command to run the project in http://localhost:3000

```bash
  npm start
```