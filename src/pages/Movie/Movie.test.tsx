/* eslint-disable testing-library/no-dom-import */
import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import renderWithRouter from '../../utils/Wrapper';
import Movie from './Movie';
import { waitFor } from '@testing-library/dom';

describe('Movie detail', () => {
  it('Should render with the correct data', async () => {
    const { container, debug, getByTestId } = renderWithRouter(<Movie />, '/movie/1');
    await waitFor(() => {
      expect(container).toHaveTextContent('Venom: Let There Be Carnage')
    });
  });
});
