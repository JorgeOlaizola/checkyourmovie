import React, { useEffect, useState } from 'react';
import {
  generateRequestToken,
  generateSessionID,
  getFavorites,
  getUserFavorites,
  getUserInfo,
} from '../../API/Requests/authentication';
import { setFavorites, setSession, setSessionId } from '../../state/actions/sessionActions';
import { MARGIN_PAGE } from '../../utils/constants';
import { useDispatch, useSelector } from 'react-redux';
import { State } from '../../state/reducers';
import { TMoviesResponse } from '../../types/responses/movies';
import { TTvShowsResponse } from '../../types/responses/tvShows';
import { Link } from 'react-router-dom';

function Session() {
  const params = new URLSearchParams(window.location.search);
  const approved = params.get('approved');
  const requestToken = params.get('request_token');
  const [success, setSuccess] = useState<boolean>();

  const dispatch = useDispatch();
  const account = useSelector((state: State) => state.session);

  useEffect(() => {
    if (approved === 'true' && requestToken) {
      generateSessionID(requestToken)
        .then((data) => {
          dispatch(setSessionId(data.session_id));
          return getUserInfo(data.session_id);
        })
        .then((data) => {
          dispatch(setSession(data));
          setSuccess(true);
        })
        .catch((error) => setSuccess(false));
    }
  }, []);

  useEffect(() => {
    if (account.sessionID && account.data) {
      getFavorites(account.sessionID, account.data.id, 'movies').then((data: TMoviesResponse) => {
        dispatch(setFavorites('movies', data.results));
      });
      getFavorites(account.sessionID, account.data.id, 'tv').then((data: TTvShowsResponse) => {
        dispatch(setFavorites('tv', data.results));
      });
    }
  }, [account.data]);

  const loginRedirect = () => {
    generateRequestToken().then((data) =>
      window.location.replace(
        `https://www.themoviedb.org/authenticate/${data.request_token}?redirect_to=https://checkyourmovie.vercel.app/session`
      )
    );
  };

  return (
    <div className={`${MARGIN_PAGE} h-screen flex justify-center items-center`}>
      <div className='p-10 bg-gray-100 md:rounded-lg w-full md:w-1/2'>
        {success ? (
          <div className='flex flex-col items-center justify-center'>
            <span className='mb-6'>Your session was approved <span className='text-green-600'>successfuly</span>. Enjoy <span className='font-bold'>CheckYourMovie!</span></span>
            <div className='flex items-center justify-around w-full'>
              <Link className='text-red-600 hover:text-red-400' to='/'>
                Home
              </Link>
              <Link className='text-red-600 hover:text-red-400' to='/explore'>
                Explore
              </Link>
              <Link className='text-red-600 hover:text-red-400' to='/profile'>
                Profile
              </Link>
            </div>
          </div>
        ) : (
          <div className='flex flex-col items-center justify-center'>
            <span className='mb-6'>Something went wrong with your request. You can still navigate as a guest</span>
            <button className='text-red-600 hover:text-red-400'onClick={loginRedirect}>Try again</button>
          </div>
        )}
      </div>
    </div>
  );
}

export default Session;
