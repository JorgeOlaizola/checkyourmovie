import React, { useEffect, useState } from 'react';
import { DEFAULT_IMAGE, IMAGES_URL_ORIGINAL } from '../../utils/constants';
import { useParams } from 'react-router-dom';
import { TDetailCredits, TDetailReviews, TDetailSimilar } from '../../types/responses/movies';
import { getTvShowDetail } from '../../API/Requests/TvShows';
import { TTvShow, TTvShowDetailContent } from '../../types/responses/tvShows';
import CircularProgressWithLabel from '../../styles/MUI/CircularProgressWithLabel';
import styled from 'styled-components';
import { useNavigate } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { State } from '../../state/reducers';
import {
  addOrRemoveFavorites,
  generateRequestToken,
  getFavorites,
} from '../../API/Requests/authentication';
import { setFavorites } from '../../state/actions/sessionActions';
import Portal from '../../components/Portal/index';

type TStyledBackground = {
  backgroundUrl: string;
};

const DetailBackground = styled.div<TStyledBackground>`
  background-image: url(${(props) => props.backgroundUrl});
  background-repeat: no-repeat;
  background-size: cover;
  background-color: rgba(0, 0, 0, 0.808);
  background-blend-mode: darken;
  background-position: center;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  /* background-attachment: fixed; */
  padding: 5rem;
`;

function TvShow() {
  const { id } = useParams();
  const [detail, setDetail] = useState<TTvShowDetailContent>();
  const [cast, setCast] = useState<TDetailCredits>();
  const [reviews, setReviews] = useState<TDetailReviews>();
  const [similar, setSimilar] = useState<TDetailSimilar<TTvShow>>();

  const navigate = useNavigate();
  const dispatch = useDispatch();
  const favorites = useSelector((state: State) => state.session.favorites.tv);
  const [isFavorite, setIsFavorite] = useState<boolean>();
  const session = useSelector((state: State) => state.session);

  const checkFavorite = () => {
    if (detail) {
      console.log('Me estoy ejecutando');
      let find = favorites.find((favorite: TTvShow) => favorite.id === detail.id);
      find ? setIsFavorite(true) : setIsFavorite(false);
    }
  };

  useEffect(() => {
    if (id) {
      getTvShowDetail(id).then((data) => {
        setDetail(data[0]);
        setCast(data[1]);
        setReviews(data[2]);
        setSimilar(data[3]);
      });
    }
  }, [id]);

  useEffect(() => {
    checkFavorite();
  }, [detail]);

  const favoriteHandler = (add: boolean, fromModal: boolean) => {
    if (!session.sessionID) return handleLoginModal(true);
    if (detail) {
      if (!add && !fromModal) return handleFavoriteModal(true);
      addOrRemoveFavorites(session.sessionID, session.data.id, 'tv', detail.id, add)
        .then(() => getFavorites(session.sessionID, session.data.id, 'tv'))
        .then((data) => {
          dispatch(setFavorites('tv', data.results));
          data.results.find((data: TTvShow) => data.id === detail.id)
            ? setIsFavorite(true)
            : setIsFavorite(false);
        })
        .catch((error) => console.log(error));
    }
  };

  const loginRedirect = () => {
    generateRequestToken().then((data) =>
      window.location.replace(
        `https://www.themoviedb.org/authenticate/${data.request_token}?redirect_to=https://checkyourmovie.vercel.app/session`
      )
    );
  };

  const [loginModal, handleLoginModal] = useState<boolean>(false);
  const [favoriteModal, handleFavoriteModal] = useState<boolean>(false);

  return (
    <div className={`w-full flex flex-col justify-center items-center`}>
      {/* CONTAINER */}
      {detail && (
        <DetailBackground
          backgroundUrl={`${
            IMAGES_URL_ORIGINAL +
            (detail?.backdrop_path ? detail?.backdrop_path : detail?.poster_path)
          }`}
          className='bg-gray-100 w-full mt-20 mb-20 flex flex-row lg:flex-col items-center justify-center text-gray-100'
        >
          <div className='flex lg:flex-row flex-col lg:items-start items-center justify-center gap-8'>
            <img
              src={`${IMAGES_URL_ORIGINAL + detail?.poster_path}`}
              className='detail-image'
              alt='tv-poster'
            />
            <div className='w-11/12 lg:w-2/3 flex flex-col'>
              <h1 className='text-4xl font-bold'>
                {detail.name}{' '}
                <span className='text-gray-300'>
                  ({new Date(detail.first_air_date).getFullYear()})
                </span>
              </h1>
              <span>
                First air date on {detail.first_air_date}{' '}
                {detail.production_companies[0]
                  ? '(' + detail.production_companies[0].origin_country + ')'
                  : null}{' '}
                •{' '}
                <span>
                  {detail.genres &&
                    detail.genres.map((genre, i) => (
                      <span>
                        {genre.name}
                        {i !== detail.genres.length - 1 && ','}{' '}
                      </span>
                    ))}
                </span>
                {/* <span>• {detail.} min</span> */}
              </span>
              <div className='flex flex-row mt-6 gap-4 items-center'>
                <div className='flex flex-row gap-2 items-center'>
                  <CircularProgressWithLabel value={detail.vote_average * 10} size={65} />
                  <span className='w-10 font-bold'>User Score</span>
                </div>
                <div className='text-red-500'>
                  {' '}
                  {isFavorite ? (
                    <svg
                      onClick={() => favoriteHandler(false, false)}
                      xmlns='http://www.w3.org/2000/svg'
                      className='h-10 w-10 cursor-pointer bg-gray-800 rounded-full p-2'
                      viewBox='0 0 20 20'
                      fill='currentColor'
                    >
                      <path
                        fillRule='evenodd'
                        d='M3.172 5.172a4 4 0 015.656 0L10 6.343l1.172-1.171a4 4 0 115.656 5.656L10 17.657l-6.828-6.829a4 4 0 010-5.656z'
                        clipRule='evenodd'
                      />
                    </svg>
                  ) : (
                    <svg
                      onClick={() => favoriteHandler(true, false)}
                      xmlns='http://www.w3.org/2000/svg'
                      className='h-10 w-10 cursor-pointer bg-gray-800 rounded-full p-2'
                      fill='none'
                      viewBox='0 0 24 24'
                      stroke='currentColor'
                    >
                      <path
                        strokeLinecap='round'
                        strokeLinejoin='round'
                        strokeWidth='2'
                        d='M4.318 6.318a4.5 4.5 0 000 6.364L12 20.364l7.682-7.682a4.5 4.5 0 00-6.364-6.364L12 7.636l-1.318-1.318a4.5 4.5 0 00-6.364 0z'
                      />
                    </svg>
                  )}
                </div>
                {detail.homepage && (
                  <svg
                    xmlns='http://www.w3.org/2000/svg'
                    onClick={() => window.location.replace(detail.homepage || '')}
                    className='h-10 w-10 bg-gray-800 rounded-full p-2 cursor-pointer'
                    fill='none'
                    viewBox='0 0 24 24'
                    stroke='currentColor'
                  >
                    <path
                      strokeLinecap='round'
                      strokeLinejoin='round'
                      strokeWidth='2'
                      d='M21 12a9 9 0 01-9 9m9-9a9 9 0 00-9-9m9 9H3m9 9a9 9 0 01-9-9m9 9c1.657 0 3-4.03 3-9s-1.343-9-3-9m0 18c-1.657 0-3-4.03-3-9s1.343-9 3-9m-9 9a9 9 0 019-9'
                    />
                  </svg>
                )}
              </div>
              <div className='text-gray-300 font-light italic'>{detail.tagline}</div>
              {detail.overview ? (
                <>
                  <p className='font-bold text-2xl mt-4'>Overview</p>
                  <p className='mt-1'>{detail.overview}</p>
                </>
              ) : null}
              {detail.production_companies && detail.production_companies.length > 0 ? (
                <>
                  <p className='font-bold text-2xl mt-4'>Produced by</p>
                  <div className='flex w-full gap-4 mt-1 flex-wrap items-center'>
                    {detail.production_companies &&
                      detail.production_companies.map((company) => (
                        <div className='flex justify-center items-center p-2 gap-2 italic font-extralight'>
                          <img
                            alt='company'
                            className='company-image'
                            src={`${
                              company.logo_path
                                ? IMAGES_URL_ORIGINAL + company.logo_path
                                : DEFAULT_IMAGE
                            }`}
                          />
                          {company.name}
                        </div>
                      ))}
                  </div>
                </>
              ) : null}
              <p className='font-bold text-2xl mt-4'>Seasons</p>
              <div className='mt-1 gap-10'>
                {detail.seasons.map((season) => (
                  <div
                    onClick={() => navigate(`/season/${detail.id}/${season.season_number}`)}
                    className='flex gap-10 w-full season-container mb-2'
                  >
                    <img
                      src={
                        season.poster_path
                          ? IMAGES_URL_ORIGINAL + season.poster_path
                          : DEFAULT_IMAGE
                      }
                      className='w-20 season-image'
                      alt='season-logo'
                    />
                    <div className='flex flex-col'>
                      <span>
                        {season.name}{' '}
                        <span className='italic font-light'>
                          (Season: {season.season_number} • {season.episode_count} episodes)
                        </span>
                      </span>
                      <span className='text-xs mt-2'>
                        {season.air_date ? `On air: ${season.air_date}` : 'No air date released'}
                      </span>
                      <p className='text-xs mt-2 italic'>{season.overview}</p>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </DetailBackground>
      )}
      <div className='flex gap-4 w-full justify-center xl:flex-row flex-col xl:items-start items-center mb-10'>
        <div className='xl:w-4/6 w-11/12 flex flex-col justify-center bg-gray-700 text-gray-100 rounded-lg p-8'>
          {cast && cast.cast ? (
            <div className='mb-4'>
              <p className='font-bold text-2xl mt-4'>Cast</p>
              <div className='mt-1 w-11/12 overflow-x-auto flex gap-4 bg-gray-900 p-8'>
                {cast.cast.map((actor) => (
                  <div
                    onClick={() => navigate(`/person/${actor.id}`)}
                    className='flex justify-start flex-col w-32 cursor-pointer h-60 hover:bg-gray-500 p-4'
                  >
                    <img
                      src={
                        actor.profile_path
                          ? IMAGES_URL_ORIGINAL + actor.profile_path
                          : DEFAULT_IMAGE
                      }
                      className='w-24'
                      alt='actor-profile'
                    />
                    <span className='w-20 text-center'>{actor.name}</span>
                    <span className='w-20 text-center itaic font-thin text-xs'>
                      as {actor.character}
                    </span>
                  </div>
                ))}
              </div>
            </div>
          ) : null}
          {cast && cast.crew ? (
            <div className='mb-4'>
              <p className='font-bold text-2xl mt-4'>Crew</p>
              <div className='mt-1 w-11/12 overflow-x-auto flex gap-4 bg-gray-900 p-8'>
                {cast.crew.map((member) => (
                  <div
                    onClick={() => navigate(`/person/${member.id}`)}
                    className='flex justify-start flex-col w-32 cursor-pointer h-60 hover:bg-gray-500 p-4'
                  >
                    <img
                      src={
                        member.profile_path
                          ? IMAGES_URL_ORIGINAL + member.profile_path
                          : DEFAULT_IMAGE
                      }
                      className='w-24'
                      alt='actor-profile'
                    />
                    <span className='w-20 text-center'>{member.name}</span>
                    <span className='w-20 text-center itaic font-thin text-xs'>
                      as {member.job}
                    </span>
                  </div>
                ))}
              </div>
            </div>
          ) : null}
          <div className='mt-4'>
            {reviews && reviews.results.length > 0 ? (
              <div className='w-full'>
                <div className='font-bold text-2xl'>Reviews</div>
                <div>
                  {reviews.results.map((review) => (
                    <div className='flex-col justify-center w-11/12 mt-10 mb-4 p-8 bg-gray-900'>
                      <div className='flex justify-between'>
                        <div className='flex justify-start items-center gap-4 mb-4'>
                          <img
                            src={
                              review.author_details.avatar_path
                                ? IMAGES_URL_ORIGINAL + review.author_details.avatar_path
                                : DEFAULT_IMAGE
                            }
                            alt='user-poster'
                            className='w-12 rounded-full'
                          />
                          <span>{review.author}</span>
                        </div>
                        <div>{review.created_at.slice(0, 10)}</div>
                      </div>
                      <div className='self-center'>{review.content}</div>
                    </div>
                  ))}
                </div>
              </div>
            ) : null}
          </div>
        </div>
        <div className='flex items-center flex-col bg-gray-700 text-gray-100 rounded-lg w-11/12 xl:w-auto p-8 max-w-lg'>
          <span className='font-bold text-2xl'>Similar tv shows</span>
          <div className='mt-4 flex-wrap'>
            {similar
              ? similar.results.slice(0, 5).map((tv) => (
                  <div
                    onClick={() => {
                      navigate(`/tv/${tv.id}`);
                      window.location.reload();
                    }}
                    className='flex gap-4 items-center mb-4 p-4 hover:bg-gray-500 cursor-pointer'
                  >
                    <img
                      alt='similar-movie'
                      src={tv.poster_path ? IMAGES_URL_ORIGINAL + tv.poster_path : DEFAULT_IMAGE}
                      className='w-24'
                    />
                    <span>{tv.name}</span>
                  </div>
                ))
              : null}
          </div>
        </div>
      </div>
      {loginModal && (
        <Portal>
          <div className='w-full h-full fixed top-0 left-0 bg-black z-50 bg-opacity-50 flex justify-center items-center'>
            <div className='w-1/2 bg-gray-900 opacity-100 text-gray-100 p-10 text-2xl flex text-center justify-between'>
              <p>
                You must{' '}
                <span
                  onClick={loginRedirect}
                  className='text-red-600 hover:text-red-400 cursor-pointer'
                >
                  LogIn
                </span>{' '}
                before add anything to favorites
              </p>
              <span
                onClick={() => handleLoginModal(false)}
                className='cursor-pointer hover:text-gray-400'
              >
                x
              </span>
            </div>
          </div>
        </Portal>
      )}
      {favoriteModal && detail ? (
        <Portal>
          <div className='w-full h-full fixed top-0 left-0 bg-black z-50 bg-opacity-50 flex justify-center items-center'>
            <div className='w-1/2 bg-gray-900 opacity-100 text-gray-100 p-10 text-2xl flex text-center flex-col gap-10'>
              <p>
                You are about to remove{' '}
                <span className='font-extrabold text-red-500'>{detail.name}</span> from your
                favorites list. Are you sure?
              </p>
              <div className='flex justify-around items-center'>
                <span
                  onClick={() => handleFavoriteModal(false)}
                  className='cursor-pointer text-black hover:text-gray-600 bg-gray-100 p-4'
                >
                  Cancel
                </span>
                <span
                  onClick={() => {
                    favoriteHandler(false, true);
                    handleFavoriteModal(false);
                  }}
                  className='bg-red-600 p-4 cursor-pointer hover:text-gray-600'
                >
                  Continue
                </span>
              </div>
            </div>
          </div>
        </Portal>
      ) : null}
    </div>
  );
}

export default TvShow;
