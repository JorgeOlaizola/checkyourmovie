import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import renderWithRouter from '../../utils/Wrapper';
import TvShow from './TvShow';
import { waitFor } from '@testing-library/react';
import { getMovieDetail } from '../../API/Requests/Movies';
import { getTvShowDetail } from '../../API/Requests/TvShows';

describe('Movie detail', () => {
  it('Should render with the correct data', async () => {
    const { container, debug, getByTestId } = renderWithRouter(<TvShow />, '/tv/1');
    await waitFor(() => {
    expect(container).toHaveTextContent('Arcane');
    });
  });
});