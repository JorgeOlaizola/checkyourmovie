import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import renderWithRouter from '../../utils/Wrapper';
import Explore from './Explore';

describe('Home page', () => {
  it('Should render properly', async () => {
    const { container, debug } = renderWithRouter(<Explore />);
    expect(container).toHaveTextContent('Search content');
  });
});
