import React, { useState, useCallback } from 'react';
import { DEFAULT_IMAGE, IMAGES_URL_ORIGINAL, MARGIN_PAGE } from '../../utils/constants';
import { search } from '../../API/Requests/search';
import { TMovie } from '../../types/responses/movies';
import { TTvShow } from '../../types/responses/tvShows';
import { debounce } from 'lodash';
import { useNavigate } from 'react-router-dom';

function Explore() {
  const [page, setPage] = useState(1);
  const [movies, setMovies] = useState<any>([]);
  const [tvShows, setTvShows] = useState<any>([]);
  const [loading, setLoading] = useState<boolean>(false);

  const navigate = useNavigate();

  const searchHandler = useCallback(
    debounce((event: React.ChangeEvent<HTMLInputElement>) => {
      if (event.target.value !== '') {
        setLoading(true);
        let moviesArray: TMovie[] = [];
        let tvShowsArray: TTvShow[] = [];
        search(event.target.value, page).then((data) => {
          data.results.forEach((item: any) => {
            if (item.media_type === 'movie') {
              moviesArray.push(item);
            }
            if (item.media_type === 'tv') {
              tvShowsArray.push(item);
            }
          });
          setMovies(moviesArray);
          setTvShows(tvShowsArray);
          setLoading(false);
        });
      } else {
        setMovies([]);
        setTvShows([]);
      }
    }, 1000),
    []
  );

  return (
    <div
      className={`${MARGIN_PAGE} min-h-screen text-gray-100 flex items-center flex-col justify-start relative mb-10`}
    >
      <div className='flex flex-col justify-center items-center gap-2 mt-8'>
        <h1 className='text-2xl font-semibold'>Search content</h1>
        <input
          type='text'
          placeholder='Title of movies or tv shows'
          className='p-4 text-black font-semibold'
          onChange={searchHandler}
        />
      </div>
      <div className='flex flex-col md:flex-row justify-around w-11/12'>
        <div>
          {movies && movies.length > 0 ? (
            <div>
              <h1 className='font-bold text-2xl mb-8'>Movies</h1>
              <div>
                {movies.map((movie: TMovie) => {
                  return (
                    <div className='p-4 bg-gray-800 mb-4 w-full md:w-96 flex items-center justify-between '>
                      <img
                        src={
                          movie.poster_path
                            ? IMAGES_URL_ORIGINAL + movie.poster_path
                            : DEFAULT_IMAGE
                        }
                        className='w-12'
                        alt='poster'
                      />
                      <span
                        onClick={() => navigate(`/movie/${movie.id}`)}
                        className='hover:text-gray-400 cursor-pointer'
                      >
                        {movie.title}
                      </span>
                    </div>
                  );
                })}
              </div>
            </div>
          ) : (
            <></>
          )}
        </div>
        <div>
          {tvShows && tvShows.length > 0 ? (
            <div>
              <h1 className='font-bold text-2xl mb-8'>Tv Shows</h1>
              <div>
                {tvShows.map((tvShow: TTvShow) => {
                  return (
                    <div className='p-4 bg-gray-800 mb-4 w-full md:w-96 flex items-center justify-between'>
                      <img
                        src={
                          tvShow.poster_path
                            ? IMAGES_URL_ORIGINAL + tvShow.poster_path
                            : DEFAULT_IMAGE
                        }
                        className='w-12'
                        alt='poster'
                      />

                      <span
                        onClick={() => navigate(`/tv/${tvShow.id}`)}
                        className='hover:text-gray-400 cursor-pointer'
                      >
                        {tvShow.name}
                      </span>
                    </div>
                  );
                })}
              </div>
            </div>
          ) : (
            <></>
          )}
        </div>
      </div>
    </div>
  );
}

export default Explore;
