import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { deleteSession, setFavorites } from '../../state/actions/sessionActions';
import { State } from '../../state/reducers';
import { DEFAULT_IMAGE, IMAGES_URL_ORIGINAL, MARGIN_PAGE } from '../../utils/constants';
import { Link, useNavigate } from 'react-router-dom';
import { TMovie } from '../../types/responses/movies';
import { addOrRemoveFavorites, getFavorites } from '../../API/Requests/authentication';
import { TTvShow } from '../../types/responses/tvShows';
import Portal from '../../components/Portal';
function Profile() {
  const profile = useSelector((state: State) => state.session);
  const favorites = useSelector((state: State) => state.session.favorites);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  useEffect(() => {
    if (!profile.data) navigate('/');
  }, [profile, dispatch]);

  const favoriteHandler = (element: TTvShow | TMovie, type: 'movies' | 'tv') => {
    handleRemove({ element, type });
    handleFavoriteModal(true);
  };

  const removeFavorite = () => {
    if (itemToRemove) {
      if (itemToRemove.element && itemToRemove?.type) {
        addOrRemoveFavorites(
          profile.sessionID,
          profile.data.id,
          itemToRemove.type === 'tv' ? 'tv' : 'movie',
          itemToRemove.element.id,
          false
        )
          .then(() => getFavorites(profile.sessionID, profile.data.id, itemToRemove.type))
          .then((data) => {
            dispatch(setFavorites(itemToRemove.type, data.results));
          })
          .catch((error) => console.log(error));
      }
    }
  };

  const [favoriteModal, handleFavoriteModal] = useState<boolean>(false);
  const [itemToRemove, handleRemove] =
    useState<{ element: TTvShow | TMovie; type: 'movies' | 'tv' }>();

  return (
    <div className={`${MARGIN_PAGE} flex justify-center items-center min-h-screen`}>
      {profile.data && (
        <div className='bg-gray-800 w-full lg:w-1/2 flex flex-col items-center justify-center mt-10 mb-10 p-8 gap-10 lg:rounded-lg text-white border-white border-2'>
          <div className='flex flex-col justify-center items-center'>
            <h1 className='text-3xl font-bold mb-4'>Profile</h1>
            <div className='flex flex-row justify-center items-center gap-10'>
              <img src={DEFAULT_IMAGE} alt='profile-logo' className='rounded-full w-44' />
              <div className='flex flex-col items-center justify-center'>
                <p>
                  <span className='font-semibold'>Username: </span>
                  <span className='italic'>{profile.data.username}</span>
                </p>
                <p>
                  <span className='font-semibold'>Name: </span>
                  <span className='italic'>
                    {profile.data.name ? profile.data.name : 'No name provided yet'}
                  </span>
                </p>
                <p>
                  <span className='font-semibold'>User ID: </span>
                  <span className='italic'>{profile.data.id}</span>
                </p>
                <p>
                  <span className='font-semibold'>Language: </span>
                  <span className='italic'>{profile.data.iso_639_1}</span>
                </p>
                <p>
                  <span className='font-semibold'>Region: </span>
                  <span className='italic'>{profile.data.iso_3166_1}</span>
                </p>
                <button
                  onClick={() => dispatch(deleteSession())}
                  className='p-2 bg-red-700 border-solid border-red-900 text-white mt-4'
                >
                  LogOut
                </button>
              </div>
            </div>
          </div>

          {favorites.movies.length > 0 || favorites.tv.length > 0 ? (
            <div className='flex flex-col items-center justify-center w-full'>
              <h1 className='text-3xl font-bold mb-6'>Favorites</h1>
              {favorites.movies.length > 0 && (
                <div className='w-full flex flex-col justify-center items-center'>
                  <h3 className='font-semibold text-2xl mb-4'>Movies</h3>
                  <div className='w-full mb-8'>
                    {favorites.movies.map((movie: TMovie) => {
                      return (
                        <div className='flex items-center justify-between w-full bg-gray-900 text-white p-4 w-full border-solid border-white mb-2'>
                          <img
                            src={
                              movie.poster_path
                                ? `${IMAGES_URL_ORIGINAL}${movie.poster_path}`
                                : DEFAULT_IMAGE
                            }
                            alt='movie-logo'
                            className='w-12'
                          />
                          <Link className='hover:text-gray-500' to={`/movie/${movie.id}`}>
                            {movie.title}
                          </Link>
                          <svg
                            onClick={() => favoriteHandler(movie, 'movies')}
                            xmlns='http://www.w3.org/2000/svg'
                            className='h-6 w-6 cursor-pointer text-red-500'
                            viewBox='0 0 20 20'
                            fill='currentColor'
                          >
                            <path
                              fillRule='evenodd'
                              d='M3.172 5.172a4 4 0 015.656 0L10 6.343l1.172-1.171a4 4 0 115.656 5.656L10 17.657l-6.828-6.829a4 4 0 010-5.656z'
                              clipRule='evenodd'
                            />
                          </svg>
                        </div>
                      );
                    })}
                  </div>
                </div>
              )}
              {favorites.tv.length > 0 && (
                <div className='w-full flex flex-col items-center justify-center'>
                  <h3 className='font-semibold text-2xl mb-4'>Tv Shows</h3>
                  <div className='w-full'>
                    {favorites.tv.map((show: TTvShow) => {
                      return (
                        <div className='flex items-center justify-between w-full bg-gray-900 text-white p-4 w-full border-solid border-white mb-2'>
                          <img
                            src={
                              show.poster_path
                                ? `${IMAGES_URL_ORIGINAL}${show.poster_path}`
                                : DEFAULT_IMAGE
                            }
                            alt='show-logo'
                            className='w-12'
                          />
                          <Link className='hover:text-gray-500' to={`/tv/${show.id}`}>
                            {show.name}
                          </Link>
                          <svg
                            onClick={() => favoriteHandler(show, 'tv')}
                            xmlns='http://www.w3.org/2000/svg'
                            className='h-6 w-6 cursor-pointer'
                            viewBox='0 0 20 20'
                            fill='currentColor'
                          >
                            <path
                              fillRule='evenodd'
                              d='M3.172 5.172a4 4 0 015.656 0L10 6.343l1.172-1.171a4 4 0 115.656 5.656L10 17.657l-6.828-6.829a4 4 0 010-5.656z'
                              clipRule='evenodd'
                            />
                          </svg>
                        </div>
                      );
                    })}
                  </div>
                </div>
              )}
            </div>
          ) : null}
        </div>
      )}
      {favoriteModal ? (
        <Portal>
          <div className='w-full h-full fixed top-0 left-0 bg-black z-50 bg-opacity-50 flex justify-center items-center'>
            <div className='w-1/2 bg-gray-900 opacity-100 text-gray-100 p-10 text-2xl flex text-center flex-col gap-10'>
              <p>Are you sure you want to remove this from favorites?</p>
              <div className='flex justify-around items-center'>
                <span
                  onClick={() => handleFavoriteModal(false)}
                  className='cursor-pointer text-black hover:text-gray-600 bg-gray-100 p-4'
                >
                  Cancel
                </span>
                <span
                  onClick={() => {
                    removeFavorite();
                    handleFavoriteModal(false);
                  }}
                  className='bg-red-600 p-4 cursor-pointer hover:text-gray-600'
                >
                  Continue
                </span>
              </div>
            </div>
          </div>
        </Portal>
      ) : null}
    </div>
  );
}

export default Profile;
