import React, { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import { getSeasonDetail, getTvShowDetail } from '../../API/Requests/TvShows';
import { TSeasonDetail, TSeasonObject, TTvShowDetailContent } from '../../types/responses/tvShows';
import { DEFAULT_IMAGE, IMAGES_URL_ORIGINAL, MARGIN_PAGE } from '../../utils/constants';
import styled from 'styled-components';
import { Params } from 'react-router'

type TStyledBackground = {
  backgroundUrl: string;
};

const DetailBackground = styled.div<TStyledBackground>`
  background-image: url(${(props) => props.backgroundUrl});
  background-repeat: no-repeat;
  background-size: cover;
  background-color: rgba(0, 0, 0, 0.808);
  background-blend-mode: darken;
  background-position: center;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  /* background-attachment: fixed; */
  padding: 5rem;
`;

function Season() {
  const { id, tvId } = useParams<string>();

  const [show, setShow] = useState<TTvShowDetailContent>();
  const [season, setSeason] = useState<TSeasonDetail>();
  const [previous, setPrevious] = useState<TSeasonObject>();
  const [next, setNext] = useState<TSeasonObject>();

  useEffect(() => {
    if (id && tvId) {
      getSeasonDetail(tvId, id).then((data) => setSeason(data));
      getTvShowDetail(tvId).then((data) => {
        setShow(data[0]);
        if (season) {
          data[0].seasons.forEach((s, index) => {
            if (s.id === season!.id) {
              if (data[0].seasons[index + 1]) {
                setNext(data[0].seasons[index + 1]);
              }
              if (data[0].seasons[index - 1]) {
                setPrevious(data[0].seasons[index - 1]);
              }
            }
          });
        }
      });
    }
  }, [season]);

  return (
    <div className={`${MARGIN_PAGE}`}>
      {season && show ? (
        <div className='relative'>
          <Link to={`/tv/${show.id}`} className='absolute text-white top-4 left-4'>
            ⬅ Back to {show.name}
          </Link>
          <DetailBackground
            backgroundUrl={IMAGES_URL_ORIGINAL + show.backdrop_path}
            className='bg-red-600 w-full flex justify-center items-center pt-6 pb-6 text-white gap-10'
          >
            <img
              src={`${IMAGES_URL_ORIGINAL}${season.poster_path}`}
              alt='season-poster'
              className='w-56'
            />
            <div className='flex flex-col w-4/6'>
              <span className='italic font-thin'>{show.name}</span>
              <span className='text-3xl'>
                <span className='font-bold'>{season.name}</span>
                <span className='italic text-2xl'> ({season.air_date})</span>
              </span>
              <span className='font-light'>{season.overview}</span>
            </div>
          </DetailBackground>
          <div className='w-full flex justify-center items-center'>
            <div className='bg-gray-700 text-gray-100 mb-8 w-full lg:w-4/6 mt-4 lg:rounded-lg p-8 flex items-center justify-center flex-col'>
              <h1 className='font-bold text-2xl mb-8'>Episodes ({season.episodes.length})</h1>
              <div className='w-5/6'>
                {season.episodes &&
                  season.episodes.map((episode) => (
                    <div key={episode.id} className='flex mb-4 gap-2'>
                      <img
                        src={episode.still_path ? IMAGES_URL_ORIGINAL + episode.still_path : DEFAULT_IMAGE}
                        alt='episode-poster'
                        className='w-48'
                      />
                      <div className='flex flex-col'>
                        <span className='text-xs'>Episode {episode.episode_number}</span>
                        <span className='font-semibold'>{episode.name}</span>
                        <p className='text-sm'>{episode.overview}</p>
                      </div>
                    </div>
                  ))}
              </div>
              <div className='flex justify-between w-11/12'>
                {previous ? (
                  <Link to={`/season/${show.id}/${previous.season_number}`}>⬅ {previous.name}</Link>
                ) : null}
                {next ? (
                  <Link to={`/season/${show.id}/${next.season_number}`}>{next.name} ➡</Link>
                ) : null}
              </div>
            </div>
          </div>
        </div>
      ) : null}
    </div>
  );
}

export default Season;
