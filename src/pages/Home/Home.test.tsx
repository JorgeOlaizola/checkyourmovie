import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import renderWithRouter from '../../utils/Wrapper';
import Home from './Home';
import { waitFor } from '@testing-library/react';

describe('Home page', () => {
  it('Should render properly', async () => {
    const { container, debug } = renderWithRouter(<Home />);
    expect(container).toHaveTextContent('Movies');
    expect(container).toHaveTextContent('TV Shows');
    await waitFor(() => {
      debug();
    });
  });
});
