import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { MARGIN_PAGE } from '../../utils/constants';
import { State } from '../../state/reducers';
import { useSelector, useDispatch } from 'react-redux';
import './Home.css';
import Filters from '../../components/Filters';
import MoviesContainer from '../../components/Movies/MoviesContainer/MoviesContainer';
import { getMovies } from '../../API/Requests/Movies';
import { addPageMovies, addPageTvShows } from '../../state/actions/cardsActions';
import Pagination from '../../components/Pagination/Pagination';
import { TCertification, TGenre } from '../../types/responses/filters';
import {
  getMovieCertifications,
  getMovieGenres,
  getTvGenres,
} from '../../API/Requests/filters';
import { TFilterObject } from '../../types/filters/filters';
import { getTvShows } from '../../API/Requests/TvShows';
import TvShowContainer from '../../components/TvShows/TvShowsContainer';

function Home() {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState<boolean>(false);
  const [display, setDisplay] = useState<'movie' | 'tv'>('movie');

  // Movies states

  const [movieGenres, setMovieGenres] = useState<TGenre[]>([]);
  const [movieCertifications, setMovieCertifications] = useState<TCertification[]>([]);
  const [currentPageMovies, setCurrentPageMovies] = useState<number>(1);
  const [totalMoviesPages, setTotalMoviesPages] = useState<number>(0);
  const [movieFilters, setMovieFilters] = useState<TFilterObject>({
    certifications: '',
    genres: '',
    year: '',
  });

  // TvShows states

  const [tvShowGenres, setTvShowGenres] = useState<TGenre[]>([]);
  const [currentPageTvShows, setCurrentPageTvShows] = useState<number>(1);
  const [totalTvShowPages, setTotalTvShowPages] = useState<number>(0);
  const [tvShowFilters, setTvShowFilters] = useState<TFilterObject>({
    certifications: '',
    genres: '',
    year: '',
  });

  useEffect(() => {
    // INIT ACTIONS
    getMovieGenres().then((data) => setMovieGenres(data));
    getMovieCertifications().then((data) => setMovieCertifications(data));

    // MOVIES
    setLoading(true);
    getMovies(movieFilters, `${currentPageMovies}`).then((data) => {
      dispatch(addPageMovies('', currentPageMovies, data.results));
      setTotalMoviesPages(data.total_pages);
      setLoading(false);
    });
  }, [currentPageMovies, movieFilters]);

  useEffect(() => {
    // INIT ACTIONS
    getTvGenres().then((data) => setTvShowGenres(data));

    // TV SHOWS
    setLoading(true);
    getTvShows(tvShowFilters, `${currentPageTvShows}`).then((data) => {
      dispatch(addPageTvShows('', currentPageTvShows, data.results));
      setTotalTvShowPages(data.total_pages);
      setLoading(false);
    });
  }, [currentPageTvShows, tvShowFilters]);

  const handleDisplay = (display: 'movie' | 'tv') => {
    setDisplay(display);
  };

  const movies = useSelector((state: State) => state.cards.movies);
  const tvShows = useSelector((state: State) => state.cards.tvShows);

  return (
    <div className={`${MARGIN_PAGE} text-red-500 mb-10 w-full`}>
      <div className='w-full bg-red-300 h-96 flex justify-center text-gray-100 items-center image-container'>
        <div className='flex justify-end items-start flex-col h-80 w-3/4'>
          <h1 className='text-5xl'>Venom: Let There Be Carnage</h1>
          <span className='mt-4'>
            After finding a host body in investigative reporter Eddie Brock, the alien symbiote must
            face a new enemy, Carnage, the alter ego of serial killer Cletus Kasady.
          </span>
          <Link
            className='text-white bg-red-600 rounded-lg p-2 hover:text-red-600 hover:bg-gray-600 self-end'
            to='/'
          >
            Check out!
          </Link>
        </div>
      </div>
      <div className='flex flex-col justify-center items-center mt-10 w-full'>
        <div className='flex flex-col items-center justify-center w-full'>
          <div className='w-full md:w-3/4 flex justify-around items-center'>
            <button
              onClick={() => handleDisplay('movie')}
              className={`text-3xl sm:text-6xl p-6 bg-gray-900 ${
                display === 'movie' && 'display-selected text-gray-100'
              }`}
            >
              Movies
            </button>
            <button
              onClick={() => handleDisplay('tv')}
              className={`text-3xl sm:text-6xl p-6 bg-gray-900 ${
                display === 'tv' && 'display-selected text-gray-100 '
              }`}
            >
              TV Shows
            </button>
          </div>
          {display === 'movie' && (
            <div className='w-full flex flex-col justify-center items-center'>
              <Filters
                handler={setMovieFilters}
                currentFilters={movieFilters}
                genres={movieGenres}
                certifications={movieCertifications}
                paginator={setCurrentPageMovies}
              />
              <MoviesContainer movies={movies[currentPageMovies]} loading={loading} type='movie' />
              <Pagination
                currentPage={currentPageMovies}
                changePage={setCurrentPageMovies}
                totalItems={totalMoviesPages}
              ></Pagination>
            </div>
          )}
          {display === 'tv' && (
            <div className='w-full flex flex-col justify-center items-center'>
              <Filters
                handler={setTvShowFilters}
                currentFilters={tvShowFilters}
                genres={tvShowGenres}
                certifications={undefined}
                paginator={setCurrentPageTvShows}
              />
              <TvShowContainer tvShows={tvShows[currentPageTvShows]} loading={loading} type='tv' />
              <Pagination
                currentPage={currentPageTvShows}
                changePage={setCurrentPageTvShows}
                totalItems={totalTvShowPages}
              ></Pagination>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}

export default Home;
