import React, { useEffect, useState } from 'react';
import { DEFAULT_IMAGE, IMAGES_URL_ORIGINAL, MARGIN_PAGE } from '../../utils/constants';
import { useParams } from 'react-router-dom';
import { TPersonDetail } from '../../types/responses/person';
import { getPersonDetail } from '../../API/Requests/person';

function Person() {
  const [person, setPerson] = useState<TPersonDetail>();
  const { id } = useParams();

  useEffect(() => {
    if (id) {
      getPersonDetail(id).then((data) => setPerson(data));
    }
  }, []);

  return (
    <div className={`${MARGIN_PAGE} w-full flex items-center justify-center min-h-screen`}>
      {person ? (
        <div className='flex flex-col bg-gray-700 text-gray-100 w-full md:w-3/4 justify-center items-center p-10 mt-10 md:rounded-lg gap-10 mb-10'>
          <div className='flex lg:flex-row flex-col items-center lg:items-start'>
            <img
              src={person.profile_path ? IMAGES_URL_ORIGINAL + person.profile_path : DEFAULT_IMAGE}
              alt='person-profile-path'
              className='w-72 max-h-96'
            />
            <div className='flex flex-col ml-8'>
              <h1 className='text-3xl font-bold'>{person.name}</h1>
              <span>Known for {person.known_for_department}</span>
              <h3 className='text-2xl font-semibold'>Biography</h3>
              <p>{person.biography}</p>
              <div className='flex gap-8'>
                <div>
                  <h4 className='text-2xl font-semibold'>Gender</h4>
                  <span>{person.gender === 2 ? 'Male' : 'Female'}</span>
                </div>
                <div>
                  <h4 className='text-2xl font-semibold'>Birthday</h4>
                  <span>{person.birthday}</span>
                </div>
                {person.deathday ? (
                  <div>
                    <h4 className='text-2xl font-semibold'>Deathday</h4>
                    <span>{person.deathday}</span>
                  </div>
                ) : null}
                <div>
                  <h4 className='text-2xl font-semibold'>Place of birthday</h4>
                  <span>{person.place_of_birth}</span>
                </div>
              </div>
              <h3 className='text-2xl font-semibold'>Also known as</h3>
              <p>{person.also_known_as.map((name) => <span className='block'>{name}</span>)}</p>
            </div>
          </div>
        </div>
      ) : null}
    </div>
  );
}

export default Person;
