import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import { useRoutes } from 'react-router-dom';
import MainLayout from './layouts/MainLayout';
import Explore from './pages/Explore/Explore';
import Home from './pages/Home/Home';
import Movie from './pages/Movie/Movie';
import Person from './pages/Person/Person';
import Season from './pages/Season/Season';
import Session from './pages/Session/Session';
import TvShow from './pages/TvShow/TvShow';
import Profile from './pages/Profile/Profile';
import routes from './utils/routes';
import { State } from './state/reducers';
import { getFavorites } from './API/Requests/authentication';
import { TTvShowsResponse } from './types/responses/tvShows';
import { TMoviesResponse } from './types/responses/movies';
import { setFavorites } from './state/actions/sessionActions';
import './styles/globals.css'

function App() {
  const dispatch = useDispatch();
  const account = useSelector((state: State) => state.session)

  useEffect(() => {
    if(account.sessionID) {
      getFavorites(account.sessionID, account.data.id, 'movies')
      .then((data: TMoviesResponse ) => {
        dispatch(setFavorites('movies', data.results))
      })
      getFavorites(account.sessionID, account.data.id, 'tv')
      .then((data: TTvShowsResponse ) => {
        dispatch(setFavorites('tv', data.results))
      })
    }
  },  [])

  const mainRoutes = {
    path: '/',
    element: <MainLayout />,
    children: [
      { path: routes.home, element: <Home /> },
      { path: routes.explore, element: <Explore /> },
      { path: routes.tvDetail, element: <TvShow /> },
      { path: routes.movieDetail, element: <Movie /> },
      { path: routes.personDetail, element: <Person /> },
      { path: routes.seasonDetail, element: <Season /> },
      { path: routes.session, element: <Session /> },
      { path: routes.profile, element: <Profile /> },
    ],
  };

  const routesContainer = useRoutes([mainRoutes]);
  return <div className='bg-gray-900 w-full'>{routesContainer}</div>;
}

export default App;
