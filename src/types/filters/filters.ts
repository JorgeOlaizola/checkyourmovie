export type TFilterObject = {
    certifications: string,
    genres: string,
    year: string | number
}