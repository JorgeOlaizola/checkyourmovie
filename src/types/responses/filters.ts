export type TCertification = {
  certification: string;
  meaning: string;
  order: number;
};

export type TCertificationsResponse = {
  certifications: {
    US: TCertification[];
  };
};

export type TGenre = {
  id: number;
  name: string;
};

export type TGenresResponse = {
  genres: TGenre[];
};
