export type TTokenResponse = {
  success: boolean;
  expires_at: string;
  request_token: string;
};

export type TSessionResponse = {
  success: boolean;
  session_id: string;
};

export type TSessionRequestBody = {
  request_token: string;
};

export type TUserInfo = {
  avatar: {
    gravatar: {
      hash: string;
    };
    tmdb: {
      avatar_path: string | null;
    };
  };
  id: number;
  iso_639_1: string;
  iso_3166_1: string;
  name: string;
  include_adult: boolean;
  username: string;
};
