import { TMovie } from "./movies";
import { TTvShow } from "./tvShows";

export type TSearch = {
    page: number;
    results: TMovie[] | TTvShow[];
    total_pages: number;
    total_results: number;
}