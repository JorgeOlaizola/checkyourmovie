import * as React from 'react';
import CircularProgress, { CircularProgressProps } from '@mui/material/CircularProgress';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';

function CircularProgressWithLabel({ value, size }: { value: number, size: number }) {
  return (
    <Box sx={{ position: 'relative' }}>
      <CircularProgress variant='determinate' color='success' value={value} size={size} />
      <Box
        sx={{
          top: 15,
          left: 14,
          position: 'absolute',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <Typography variant='caption' component='div' color='white' fontSize='1.2rem'>{`${Math.round(
          value
        )}%`}</Typography>
      </Box>
    </Box>
  );
}

export default CircularProgressWithLabel;
