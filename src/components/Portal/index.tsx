import React, { useEffect } from 'react';
import ReactDOM from 'react-dom';

const portalRoot = document.getElementById('portal');

function Portal({ children }: { children: any }) {
  const el = document.createElement('div');

  useEffect(() => {
    portalRoot?.appendChild(el);

    return () => {
      portalRoot?.removeChild(el);
    };
  }, [el]);
  return ReactDOM.createPortal(children, el);
}

export default Portal;
