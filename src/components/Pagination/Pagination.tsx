import React from 'react';
import './Pagination.css'

function Pagination({
  totalItems,
  currentPage,
  changePage,
}: {
  totalItems: number;
  currentPage: number;
  changePage: Function;
}) {
  const pagesArray: number[] = []
  for (let i = 1; i <= totalItems; i++) {
    pagesArray.push(i);
  }

  return <div className='gap-6 flex text-white font-extrabold bg-opacity-50 bg-black p-4'>
      { currentPage !== 1 && <button className={`p-1 md:p-4 `} onClick={() => changePage(currentPage - 1)}>{'<'}</button>}
      { currentPage === 1  && pagesArray.slice(0, currentPage + 5).map((page: number) => <button className={`p-2 rounded-full ${page === currentPage ? 'item-selected' : ''}`} onClick={() => changePage(page)}>{page}</button>)}
      { currentPage === 2  && pagesArray.slice(currentPage - 2, currentPage + 4).map((page: number) => <button className={`p-2 rounded-full ${page === currentPage ? 'item-selected' : ''}`} onClick={() => changePage(page)}>{page}</button>)}
      { currentPage >= 3  && pagesArray.slice(currentPage - 3, currentPage + 2).map((page: number) => <button className={`p-2 rounded-full ${page === currentPage ? 'item-selected' : ''}`} onClick={() => changePage(page)}>{page}</button>)}
      { currentPage !== totalItems && <button className={`p-1 md:p-4 `} onClick={() => changePage(currentPage + 1)}>{'>'}</button>}
  </div>;
}

export default Pagination;
