import React, { useCallback } from 'react';
import { TFilterObject } from '../../types/filters/filters';
import { TCertification, TGenre } from '../../types/responses/filters';
import { debounce } from 'lodash';

function Filters({
  genres,
  certifications,
  handler,
  currentFilters,
  paginator,
}: {
  genres: TGenre[];
  certifications: TCertification[] | void;
  handler: Function;
  currentFilters: TFilterObject;
  paginator: Function;
}) {
  const genresHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (!currentFilters.genres) {
      paginator(1);
      return handler({ ...currentFilters, genres: event.target.value });
    } else if (currentFilters.genres.includes(event.target.value)) {
      const genresQuery = currentFilters.genres.split(',');
      const newQuery = genresQuery.filter((genre) => genre !== event.target.value).join(',');
      paginator(1);
      return handler({ ...currentFilters, genres: newQuery });
    } else {
      paginator(1);
      return handler({
        ...currentFilters,
        genres: currentFilters.genres + `,${event.target.value}`,
      });
    }
  };

  const certificationsHandler = (event: React.ChangeEvent<HTMLSelectElement>) => {
    paginator(1);
    if (event.target.value === 'none') return handler({ ...currentFilters, certifications: '' });
    return handler({ ...currentFilters, certifications: event.target.value });
  };

  const yearHandler = useCallback(
    debounce((event: React.ChangeEvent<HTMLInputElement>) => {
      paginator(1);
      handler({ ...currentFilters, year: event.target.value });
    }, 1000),
    []
  );

  return (
    <div className='bg-gray-800 flex flex-col md:flex-row justify-around p-10 w-full xl:w-3/4 mt-10 mb-10 gap-4 text-gray-100 font-bold'>
      <div className='flex flex-col items-center justify-center w-full md:w-3/6'>
        <label className='self-center text-3xl mb-4 text-red-500'>Genres</label>
        <div className='flex w-full gap-4 flex-wrap'>
          {genres &&
            genres.map((genre) => (
              <div className='flex justify-center items-center gap-1'>
                <input onChange={genresHandler} type='checkbox'  value={genre.id} />
                <label className={currentFilters.genres.includes(`${genre.id}`) ? 'text-red-500' : ''}>{genre.name}</label>
              </div>
            ))}
        </div>
      </div>
      {certifications && (
        <div className='flex flex-col'>
          <label className='self-center text-3xl mb-4 text-red-500'>Certifications</label>
          <select className='text-red-500 font-semibold' onChange={certificationsHandler}>
            <option value='none'>Default</option>
            {certifications.map((c) => (
              <option value={c.certification}>{c.certification}</option>
            ))}
          </select>
        </div>
      )}
      <div className='flex flex-col'>
        <label className='self-center text-3xl mb-4 text-red-500'>Release year</label>
        <input
          onChange={yearHandler}
          type='number'
          placeholder='Type the year'
          className='p-2 text-red-500 font-semibold'
          min={1901}
          minLength={4}
        ></input>
      </div>
    </div>
  );
}

export default Filters;
