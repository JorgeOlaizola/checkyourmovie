import React from 'react';
import TvShow from '../../../components/TvShows/TvShow';
import { TTvShow } from '../../../types/responses/tvShows';
import './TvShowsContainer.css';
import { useSelector } from 'react-redux';
import { State } from '../../../state/reducers';

function CardContainer({
  tvShows,
  loading,
  type,
}: {
  tvShows: TTvShow[];
  loading: boolean;
  type: 'movie' | 'tv';
}) {
  const favorites = useSelector((state: State) => state.session.favorites.tv);
  return (
    <div>
      <div className='grid-container-movies mt-10 mb-10'>
        {loading ? (
          <span>Loading...</span>
        ) : tvShows ? (
          tvShows.map((tvShow) => {
            let isFavorite = favorites.find((favorite: TTvShow) => tvShow.id === favorite.id);
            return <TvShow key={tvShow.id} tvShow={tvShow} favorite={isFavorite ? true : false} />;
          })
        ) : (
          <span>Loading...</span>
        )}
      </div>
    </div>
  );
}

export default CardContainer;
