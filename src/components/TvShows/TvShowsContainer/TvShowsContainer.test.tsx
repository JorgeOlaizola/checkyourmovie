import renderWithRouter from '../../../utils/Wrapper';
import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import TvShowsContainer from './index';
import { getTvShows } from '../../../API/Requests/TvShows';

describe('TvShows container', () => {
  it('Should render properly', async () => {
    await getTvShows({ year: '', genres: '', certifications: '' }).then((data) => {
      const { container,  } = renderWithRouter(
        <TvShowsContainer loading={false} type='tv' tvShows={data.results} />
      );
      expect(container).toHaveTextContent('The Walking Dead');
    });
  });
});
