import React from 'react';
import { DEFAULT_IMAGE, LOGO } from '../../utils/constants';
import { Link } from 'react-router-dom';
import { generateRequestToken } from '../../API/Requests/authentication';
import { useSelector } from 'react-redux';
import { State } from '../../state/reducers';
import './Navbar.css'

function Navbar() {
  const clickFunction = () => {
    const mobileMenu = document.querySelector('.mobile-menu')!;
    mobileMenu.classList.toggle('hidden');
  };

  const session = useSelector((state: State) => state.session.data);

  const loginRedirect = () => {
    generateRequestToken().then((data) =>
      window.location.replace(
        `https://www.themoviedb.org/authenticate/${data.request_token}?redirect_to=https://checkyourmovie.vercel.app/session`
      )
    );
  };

  return (
    <nav className='w-full navbar-bg flex flex-col justify-center items-center fixed shadow-inner top-0 z-50'>
      <div className='flex justify-between items-center text-gray-100 w-3/4 '>
        <Link to='/'>
          <img src={LOGO} alt='logo' className='max-h-20' />
        </Link>
        <ul className='gap-10 hidden md:flex'>
          <Link className='hover:text-red-500' to='/'>
            Home
          </Link>
          <Link className='hover:text-red-500' to='/explore'>
            Explore
          </Link>
        </ul>
        {session ? (
          <Link to='/profile' className='hidden md:flex cursor-pointer p-4 text-white'>
            <div className='flex justify-center items-center gap-2'>
              <img src={DEFAULT_IMAGE} className='w-10 rounded-full' alt='profile-poster' />
              <span>{session.username}</span>
            </div>
          </Link>
        ) : (
          <div
            onClick={loginRedirect}
            className='hidden md:flex cursor-pointer bg-red-700 p-2  login-button'
          >
            Login
          </div>
        )}
        <div
          onClick={clickFunction}
          style={{ cursor: 'pointer' }}
          className='md:hidden flex items-center mobile-menu-button'
        >
          <svg
            xmlns='http://www.w3.org/2000/svg'
            className='h-6 w-6'
            fill='none'
            viewBox='0 0 24 24'
            stroke='currentColor'
          >
            <path
              strokeLinecap='round'
              strokeLinejoin='round'
              strokeWidth='2'
              d='M4 6h16M4 12h16M4 18h16'
            />
          </svg>
        </div>
      </div>
      <div className='hidden md:hidden mobile-menu mt-3 w-11/12 text-gray-100 pb-4'>
        <Link className='block py-2 px-4 text-sm hover:bg-gray-600' to='/'>
          Home
        </Link>
        <Link className='block py-2 px-4 text-sm hover:bg-gray-600' to='/explore'>
          Explore
        </Link>
        {session ? (
          <Link to='/profile' className='block py-2 px-4 text-sm hover:bg-gray-600 cursor-pointer'>
            Profile
          </Link>
        ) : (
          <div onClick={loginRedirect} className='block py-2 px-4 text-sm hover:bg-gray-600 cursor-pointer'>Login</div>
        )}
      </div>
    </nav>
  );
}

export default Navbar;
