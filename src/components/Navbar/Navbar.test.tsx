import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import renderWithRouter from '../../utils/Wrapper';
import Navbar from './Navbar';

describe('Navbar', () => {
  it('Should render properly', async () => {
      const { container } = renderWithRouter(<Navbar />)
      expect(container).toHaveTextContent('Home')
      expect(container).toHaveTextContent('Explore')
      expect(container).toHaveTextContent('Login')
  });
});
