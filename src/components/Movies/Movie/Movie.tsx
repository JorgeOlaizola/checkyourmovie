import React, { useState } from 'react';
import { TMovie } from '../../../types/responses/movies';
import { IMAGES_URL } from '../../../utils/constants';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { State } from '../../../state/reducers';
import './Movie.css';
import {
  addOrRemoveFavorites,
  generateRequestToken,
  getFavorites,
} from '../../../API/Requests/authentication';
import { setFavorites } from '../../../state/actions/sessionActions';
import Portal from '../../Portal';

function Card({ movie, favorite }: { movie: TMovie; favorite: boolean }) {
  const { title, poster_path, id } = movie;
  const navigate = useNavigate();
  const session = useSelector((state: State) => state.session);
  const dispatch = useDispatch();

  const favoriteHandler = (add: boolean, fromModal: boolean) => {
    if (!session.sessionID) return handleLoginModal(true);
    if (!add && !fromModal) return handleFavoriteModal(true);
    addOrRemoveFavorites(session.sessionID, session.data.id, 'movie', movie.id, add)
      .then(() => getFavorites(session.sessionID, session.data.id, 'movies'))
      .then((data) => {
        dispatch(setFavorites('movies', data.results));
      })
      .catch((error) => console.log(error));
  };

  const loginRedirect = () => {
    generateRequestToken().then((data) =>
      window.location.replace(
        `https://www.themoviedb.org/authenticate/${data.request_token}?redirect_to=https://checkyourmovie.vercel.app/session`
      )
    );
  };

  const [loginModal, handleLoginModal] = useState<boolean>(false);
  const [favoriteModal, handleFavoriteModal] = useState<boolean>(false);

  return (
    <div className='movie-container bg-gray-800 flex-col'>
      <img
        onClick={() => navigate(`/movie/${id}`)}
        src={`${IMAGES_URL}${poster_path}`}
        alt='movie-poster'
        className='max-h-80 md:max-h-96 w-full cursor-pointer movie-img'
      />
      <div className='flex flex-col justify-center items-center w-full'>
        <h1 className='text-gray-100 font-bold text-center text-xl movie-title'>{title}</h1>
        <div className='flex w-11/12 justify-evenly items-center'>
          <button>
            {favorite ? (
              <svg
                onClick={() => favoriteHandler(false, false)}
                xmlns='http://www.w3.org/2000/svg'
                className='h-6 w-6 cursor-pointer'
                viewBox='0 0 20 20'
                fill='currentColor'
              >
                <path
                  fillRule='evenodd'
                  d='M3.172 5.172a4 4 0 015.656 0L10 6.343l1.172-1.171a4 4 0 115.656 5.656L10 17.657l-6.828-6.829a4 4 0 010-5.656z'
                  clipRule='evenodd'
                />
              </svg>
            ) : (
              <svg
                onClick={() => favoriteHandler(true, false)}
                xmlns='http://www.w3.org/2000/svg'
                className='h-6 w-6'
                fill='none'
                viewBox='0 0 24 24'
                stroke='currentColor'
              >
                <path
                  strokeLinecap='round'
                  strokeLinejoin='round'
                  strokeWidth='2'
                  d='M4.318 6.318a4.5 4.5 0 000 6.364L12 20.364l7.682-7.682a4.5 4.5 0 00-6.364-6.364L12 7.636l-1.318-1.318a4.5 4.5 0 00-6.364 0z'
                />
              </svg>
            )}
          </button>
          <button onClick={() => navigate(`/movie/${id}`)} className='text-blue-500'>
            <svg
              xmlns='http://www.w3.org/2000/svg'
              className='h-5 w-5 cursor-pointer'
              viewBox='0 0 20 20'
              fill='currentColor'
            >
              <path
                fillRule='evenodd'
                d='M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z'
                clipRule='evenodd'
              />
            </svg>
          </button>
        </div>
      </div>
      {loginModal && (
        <Portal>
          <div className='w-full h-full fixed top-0 left-0 bg-black z-50 bg-opacity-50 flex justify-center items-center'>
            <div className='w-1/2 bg-gray-900 opacity-100 text-gray-100 p-10 text-2xl flex text-center justify-between'>
              <p>
                You must{' '}
                <span
                  onClick={loginRedirect}
                  className='text-red-600 hover:text-red-400 cursor-pointer'
                >
                  LogIn
                </span>{' '}
                before add anything to favorites
              </p>
              <span
                onClick={() => handleLoginModal(false)}
                className='cursor-pointer hover:text-gray-400'
              >
                x
              </span>
            </div>
          </div>
        </Portal>
      )}
      {favoriteModal && (
        <Portal>
          <div className='w-full h-full fixed top-0 left-0 bg-black z-50 bg-opacity-50 flex justify-center items-center'>
            <div className='w-1/2 bg-gray-900 opacity-100 text-gray-100 p-10 text-2xl flex text-center flex-col gap-10'>
              <p>
                You are about to remove <span className='font-extrabold text-red-500'>{title}</span>{' '}
                from your favorites list. Are you sure?
              </p>
              <div className='flex justify-around items-center'>
                <span
                  onClick={() => handleFavoriteModal(false)}
                  className='cursor-pointer text-black hover:text-gray-600 bg-gray-100 p-4'
                >
                  Cancel
                </span>
                <span
                  onClick={() => {
                    favoriteHandler(false, true);
                    handleFavoriteModal(false);
                  }}
                  className='bg-red-600 p-4 cursor-pointer hover:text-gray-600'
                >
                  Continue
                </span>
              </div>
            </div>
          </div>
        </Portal>
      )}
    </div>
  );
}

export default Card;
