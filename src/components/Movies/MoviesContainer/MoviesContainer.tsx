import React from 'react';
import Movie from '../../../components/Movies/Movie/Movie';
import { TMovie } from '../../../types/responses/movies';
import { useSelector } from 'react-redux'
import './MoviesContainer.css';
import { State } from '../../../state/reducers';

function CardContainer({
  movies,
  loading,
  type,
}: {
  movies: TMovie[];
  loading: boolean;
  type: 'movie' | 'tv';
}) {
  const favorites = useSelector((state: State) => state.session.favorites.movies)
  return (
    <div className='w-full flex justify-center items-center'>
      <div className='grid-container-movies mt-10 mb-10'>
        {loading ? (
          <span>Loading...</span>
        ) : movies ? (
          movies.map((movie) => {
          let isFavorite = favorites.find((favorite: TMovie) => movie.id === favorite.id)
          return (<Movie key={movie.id} movie={movie} favorite={isFavorite ? true : false} />)
        })
        ) : (
          <span>Loading...</span>
        )}
      </div>
    </div>
  );
}

export default CardContainer;
