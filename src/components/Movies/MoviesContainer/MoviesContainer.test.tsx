import renderWithRouter from '../../../utils/Wrapper';
import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import MoviesContainer from './MoviesContainer';
import { getMovies } from '../../../API/Requests/Movies';

describe('Movies container', () => {
  it('Should render properly', async () => {
    await getMovies({ year: '', genres: '', certifications: '' }).then((data) => {
      const { container } = renderWithRouter(
        <MoviesContainer loading={false} type='tv' movies={data.results} />
      );
      expect(container).toHaveTextContent('Venom: Let There Be Carnage');
    });
  });
});
