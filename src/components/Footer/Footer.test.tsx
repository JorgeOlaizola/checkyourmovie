import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import renderWithRouter from '../../utils/Wrapper';
import Footer from './Footer'

describe('Footer', () => {
  it('Should render properly', async () => {
      const { container } = renderWithRouter(<Footer />)
      expect(container).toHaveTextContent('Created by Jorge Olaizola')
  });
});
