import React from 'react';
import '../Navbar/Navbar.css';

function Footer() {
  return (
    <div className='footer-bg flex justify-center items-center text-red-600 pt-10 pb-24'>
      Created by Jorge Olaizola
    </div>
  );
}

export default Footer;
