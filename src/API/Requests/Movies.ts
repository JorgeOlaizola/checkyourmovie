import { TFilterObject } from '../../types/filters/filters';
import {
  TDetailContent,
  TDetailCredits,
  TDetailReviews,
  TDetailSimilar,
  TMovie,
  TMoviesResponse,
} from '../../types/responses/movies';
import APIRoutes from '../APIRoutes';
import { get } from '../Fetch';

export const getMovies = async (filters: TFilterObject, page = '1') => {
  let url = `${process.env.REACT_APP_BASE_URL}/discover/${
    APIRoutes.movies
  }?sort_by=popularity.desc&api_key=${process.env.REACT_APP_API_KEY}&page=${page}${
    filters.certifications ? `&certification_country=US&certification=${filters.certifications}` : ''
  }${filters.genres ? `&with_genres=${filters.genres}` : ''}${filters.year ? `&year=${filters.year}` : '' }`;
  return await get<TMoviesResponse>(url);
};

export const getMovieDetail = async (id: string) => {
  return await Promise.all([
    get<TDetailContent>(
      `${process.env.REACT_APP_BASE_URL}/${APIRoutes.movies}/${id}?api_key=${process.env.REACT_APP_API_KEY}`
    ),
    get<TDetailCredits>(
      `${process.env.REACT_APP_BASE_URL}/${APIRoutes.movies}/${id}/credits?api_key=${process.env.REACT_APP_API_KEY}`
    ),
    get<TDetailReviews>(
      `${process.env.REACT_APP_BASE_URL}/${APIRoutes.movies}/${id}/reviews?api_key=${process.env.REACT_APP_API_KEY}`
    ),

    get<TDetailSimilar<TMovie>>(
      `${process.env.REACT_APP_BASE_URL}/${APIRoutes.movies}/${id}/similar?api_key=${process.env.REACT_APP_API_KEY}`
    ),
  ]);
};
