import { TMoviesResponse } from '../../types/responses/movies';
import { getMovies, getMovieDetail } from './Movies';
import { getPersonDetail } from './person';
import { search } from './search';
import { getSeasonDetail, getTvShowDetail, getTvShows } from './TvShows';

describe('FETCH: Movies', () => {
  it('getMovies should answer with the right data', async () => {
    const response: TMoviesResponse = await getMovies({ genres: '', certifications: '', year: ''});
    expect(response.page).toEqual(1);
    expect(response.results.length).toEqual(response.total_results);
    expect(response.page).toEqual(response.total_pages);
  });
});

describe('FETCH: Movies Detail', () => {
  it('getMovieDetail should answer with the right data', async () => {
    const response = await getMovieDetail('1');
    expect(response[0].title).toBe('Venom: Let There Be Carnage');
    expect(response[1].cast).toHaveLength(3);
    expect(response[1].crew).toHaveLength(2);
    expect(response[2].results).toHaveLength(2);
    expect(response[3].results).toHaveLength(5);
  });
});

describe('FETCH: Person Detail', () => {
  it('getPersonDetail should answer with the right data', async () => {
    const response = await getPersonDetail('1');
    expect(response.name).toBe('George Lucas');
    expect(response.gender).toEqual(2);
  });
});

describe('FETCH: Search', () => {
  it('search should answer the right data based on the query', async () => {
    const response = await search('batman');
    expect(response.results).toHaveLength(3);
    expect(response.results[0].title).toBe('Batman');
  });
});

describe('FETCH: Tv Shows', () => {
  it('getTvShows should answer with the right data', async () => {
    const response = await getTvShows({ genres: '', certifications: '', year: ''});
    expect(response.page).toEqual(response.total_pages);
    expect(response.results.length).toEqual(response.total_results);
    expect(response.results[0].name).toBe('Dr. Brain');
  });
  it('getSeasonDetail should answer with the right data', async () => {
    const response = await getSeasonDetail('1', '1');
    expect(response.id).toEqual(1);
    expect(response.name).toBe('Season 1');
  });
  it('getTvShowDetail should answer with the right data', async () => {
    const response = await getTvShowDetail('1');
    expect(response[0].name).toBe('Arcane')
    expect(response[1].cast).toHaveLength(1);
    expect(response[1].crew).toHaveLength(1);
    expect(response[2].results).toHaveLength(1);
    expect(response[3].results).toHaveLength(5);
  })
});
