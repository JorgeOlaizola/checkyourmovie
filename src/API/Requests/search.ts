import { get } from '../Fetch';
import APIRoutes from '../APIRoutes';
import { TSearch } from '../../types/responses/search';

export const search = async (query = '', page = 1) => {
  return await get<TSearch>(
    `${process.env.REACT_APP_BASE_URL}/${APIRoutes.search}?api_key=${process.env.REACT_APP_API_KEY}&page=${page}&query=${query}&include_adult=false`
  )
};
