import { TFilterObject } from '../../types/filters/filters';
import { TDetailCredits, TDetailReviews, TDetailSimilar } from '../../types/responses/movies';
import {
  TSeasonDetail,
  TTvShow,
  TTvShowDetailContent,
  TTvShowsResponse,
} from '../../types/responses/tvShows';
import APIRoutes from '../APIRoutes';
import { get } from '../Fetch';

export const getTvShows = async (filters: TFilterObject, page = '1') => {
  let url = `${process.env.REACT_APP_BASE_URL}/discover/${
    APIRoutes.tvShows
  }?sort_by=popularity.desc&api_key=${process.env.REACT_APP_API_KEY}&page=${page}${
    filters.certifications
      ? `&certification_country=US&certification=${filters.certifications}`
      : ''
  }${filters.genres ? `&with_genres=${filters.genres}` : ''}${
    filters.year ? `&first_air_date_year=${filters.year}` : ''
  }`;
  return await get<TTvShowsResponse>(url);
};

export const getTvShowDetail = async (id: string) => {
  return await Promise.all([
    get<TTvShowDetailContent>(
      `${process.env.REACT_APP_BASE_URL}/${APIRoutes.tvShows}/${id}?api_key=${process.env.REACT_APP_API_KEY}`
    ),
    get<TDetailCredits>(
      `${process.env.REACT_APP_BASE_URL}/${APIRoutes.tvShows}/${id}/credits?api_key=${process.env.REACT_APP_API_KEY}`
    ),
    get<TDetailReviews>(
      `${process.env.REACT_APP_BASE_URL}/${APIRoutes.tvShows}/${id}/reviews?api_key=${process.env.REACT_APP_API_KEY}`
    ),
    get<TDetailSimilar<TTvShow>>(
      `${process.env.REACT_APP_BASE_URL}/${APIRoutes.tvShows}/${id}/similar?api_key=${process.env.REACT_APP_API_KEY}`
    ),
  ]);
};

export const getSeasonDetail = async (id: string, season: string) => {
  return await get<TSeasonDetail>(
    `${process.env.REACT_APP_BASE_URL}/${APIRoutes.tvShows}/${id}/season/${season}?api_key=${process.env.REACT_APP_API_KEY}`
  );
};
