import {
  TSessionRequestBody,
  TSessionResponse,
  TTokenResponse,
  TUserInfo,
} from '../../types/responses/authentication';
import { TMoviesResponse } from '../../types/responses/movies';
import { TTvShowsResponse } from '../../types/responses/tvShows';
import { get, post } from '../Fetch';

const headers = {
  headers: {
    Authorization: `Bearer ${process.env.REACT_APP_API_KEY}`,
    'Content-Type': 'application/json;charset=UTF-8',
  },
};

export const generateRequestToken = () => {
  return get<TTokenResponse>(
    `${process.env.REACT_APP_BASE_URL}/authentication/token/new?api_key=${process.env.REACT_APP_API_KEY}`
  );
};

export const generateSessionID = (requestToken: string) => {
  return post<TSessionRequestBody, TSessionResponse>(
    `${process.env.REACT_APP_BASE_URL}/authentication/session/new?api_key=${process.env.REACT_APP_API_KEY}`,
    { request_token: requestToken },
    headers
  ).catch(() => {
    throw new Error('Error');
  });
};

export const getUserInfo = (sessionID: string) => {
  return get<TUserInfo>(
    `${process.env.REACT_APP_BASE_URL}/account?api_key=${process.env.REACT_APP_API_KEY}&session_id=${sessionID}`
  );
};

export const getUserFavorites = (sessionID: string, accountID: string) => {
  return Promise.all([
    get<any>(
      `${process.env.REACT_APP_BASE_URL}/account/${accountID}/favorite/movies?api_key=${process.env.REACT_APP_API_KEY}&session_id=${sessionID}`
    ),
    get<any>(
      `${process.env.REACT_APP_BASE_URL}/account/${accountID}/favorite/movies?api_key=${process.env.REACT_APP_API_KEY}&session_id=${sessionID}`
    ),
  ]);
};

export const addOrRemoveFavorites = (
  sessionID: string,
  accountID: number,
  type: 'movie' | 'tv',
  id: number,
  add: boolean
) => {
  const requestBody = {
    media_type: type,
    media_id: id,
    favorite: add,
  };
  return post<any, any>(
    `${process.env.REACT_APP_BASE_URL}/account/${accountID}/favorite?session_id=${sessionID}&api_key=${process.env.REACT_APP_API_KEY}`,
    requestBody,
    headers
  );
};

export const getFavorites = (sessionID: string, accountID: number, type: 'movies' | 'tv') => {
  return get<TMoviesResponse & TTvShowsResponse>(
    `${process.env.REACT_APP_BASE_URL}/account/${accountID}/favorite/${type}?api_key=${process.env.REACT_APP_API_KEY}&session_id=${sessionID}`
  );
};
