import { TCertificationsResponse, TGenresResponse } from '../../types/responses/filters';
import APIRoutes from '../APIRoutes';
import { get } from '../Fetch';

const { certification, list, movies, tvShows, genre } = APIRoutes;

export const getMovieCertifications = () => {
  return get<TCertificationsResponse>(
    `${process.env.REACT_APP_BASE_URL}/${certification}/${movies}/${list}?api_key=${process.env.REACT_APP_API_KEY}`
  ).then((data) => data.certifications.US);
};

export const getTvCertifications = () => {
  return get<TCertificationsResponse>(
    `${process.env.REACT_APP_BASE_URL}/${certification}/${tvShows}/${list}?api_key=${process.env.REACT_APP_API_KEY}`
  ).then((data) => data.certifications.US);
};

export const getMovieGenres = () => {
  return get<TGenresResponse>(
    `${process.env.REACT_APP_BASE_URL}/${genre}/${movies}/${list}?api_key=${process.env.REACT_APP_API_KEY}`
  ).then((data) => data.genres);
};

export const getTvGenres = () => {
  return get<TGenresResponse>(
    `${process.env.REACT_APP_BASE_URL}/${genre}/${tvShows}/${list}?api_key=${process.env.REACT_APP_API_KEY}`
  ).then((data) => data.genres);
};
