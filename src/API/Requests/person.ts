import { get } from '../Fetch';
import APIRoutes from '../APIRoutes';
import { TPersonDetail } from '../../types/responses/person';

export const getPersonDetail = async (id: string) => {
  return await get<TPersonDetail>(
    `${process.env.REACT_APP_BASE_URL}/${APIRoutes.person}/${id}?api_key=${process.env.REACT_APP_API_KEY}`
  )
};
