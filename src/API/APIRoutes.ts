const APIRoutes = {
  movies: 'movie',
  tvShows: 'tv',
  search: 'search/multi',
  person: 'person',
  certification: 'certification',
  list: 'list',
  genre: 'genre'
};

export default APIRoutes;
