import { combineReducers } from 'redux';
import sessionReducer from './sessionReducer';
import cardsReducer from './cardsReducer';

const reducers = combineReducers({
  session: sessionReducer,
  cards: cardsReducer
});

export default reducers;

export type State = ReturnType<typeof reducers>
