import { Action } from 'redux';
import { TMovie } from '../../types/responses/movies';
import { TTvShow } from '../../types/responses/tvShows';
import { cardActions } from '../actions';

type TCardStates = {
  movies: {
    [key: number]: TMovie[];
  };
  tvShows: {
    [key: number]: TTvShow[];
  };
};

const initialState: TCardStates = {
  movies: {},
  tvShows: {},
};

const cardsReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case cardActions.ADD_PAGE_MOVIES:
      return {
        ...state,
        movies: { ...state.movies, [action.payload.page]: action.payload.content },
      };
    case cardActions.ADD_PAGE_TV_SHOWS:
      return {
        ...state,
        tvShows: { ...state.tvShows, [action.payload.page]: action.payload.content },
      };
    default:
      return state;
  }
};

export default cardsReducer;
