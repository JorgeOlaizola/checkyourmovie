import { TUserInfo } from '../../types/responses/authentication';
import { TMovie } from '../../types/responses/movies';
import { TTvShow } from '../../types/responses/tvShows';
import { sessionActions } from '../actions';

type TSessionReducer = {
  data: TUserInfo | void;
  favorites: {
    movies: TMovie[];
    tv: TTvShow[];
  };
  sessionID: string;
};
const initialState: TSessionReducer = {
  data: undefined,
  favorites: {
    movies: [],
    tv: [],
  },
  sessionID: '',
};

const sessionReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case sessionActions.SET_SESSION:
      return { ...state, data: action.payload };
    case sessionActions.DELETE_SESSION:
      return initialState;
    case sessionActions.SET_FAVORITES_MOVIES:
      return { ...state, favorites: { ...state.favorites, movies: action.payload } };
    case sessionActions.SET_FAVORITES_TV:
      return { ...state, favorites: { ...state.favorites, tv: action.payload } };
    case sessionActions.SET_SESSION_ID:
      return { ...state, sessionID: action.payload };
    default:
      return state;
  }
};

export default sessionReducer;
