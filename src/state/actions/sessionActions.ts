import { Dispatch } from 'redux';
import { sessionActions } from '.';
import { TUserInfo } from '../../types/responses/authentication';
import { TMovie } from '../../types/responses/movies';
import { TTvShow } from '../../types/responses/tvShows';

export const setSession = (session: TUserInfo) => {
  return (dispatch: Dispatch) => {
    dispatch({ type: sessionActions.SET_SESSION, payload: session });
  };
};

export const deleteSession = () => {
  return (dispatch: Dispatch) => {
    dispatch({ type: sessionActions.DELETE_SESSION });
  };
};

export const setSessionId = (sessionID: string) => {
  return (dispatch: Dispatch) => {
    dispatch({ type: sessionActions.SET_SESSION_ID, payload: sessionID });
  };
};

export const setFavorites = (type: 'movies' | 'tv', payload: TTvShow[] | TMovie[]) => {
  return (dispatch: Dispatch) => {
    type === 'movies'
      ? dispatch({ type: sessionActions.SET_FAVORITES_MOVIES, payload })
      : dispatch({ type: sessionActions.SET_FAVORITES_TV, payload });
  };
};
