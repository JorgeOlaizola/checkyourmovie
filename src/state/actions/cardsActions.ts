import { Dispatch } from 'redux';
import { cardActions } from '.';

export const addPageMovies = <T>(type: string, page: number, payload: T) => {
  return (dispatch: Dispatch) => {
    dispatch({ type: cardActions.ADD_PAGE_MOVIES, payload: { content: payload, page } });
  };
};

export const addPageTvShows = <T>(type: string, page: number, payload: T) => {
  return (dispatch: Dispatch) => {
    dispatch({ type: cardActions.ADD_PAGE_TV_SHOWS, payload: { content: payload, page } });
  };
};