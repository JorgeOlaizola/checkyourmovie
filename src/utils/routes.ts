const routes = {
    home: '/',
    explore: '/explore',
    movieDetail: '/movie/:id',
    tvDetail: '/tv/:id',
    seasonDetail: '/season/:tvId/:id',
    personDetail: '/person/:id',
    session: '/session',
    profile: '/profile'
}

export default routes;