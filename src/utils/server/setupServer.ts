import { setupServer } from 'msw/node';
import { rest } from 'msw';
import { movies, movieDetail } from './responses/moviesResponse';
import APIRoutes from '../../API/APIRoutes';
import { personDetail } from './responses/personResponse';
import { search } from './responses/searchResponse';
import { seasonDetail, tvShowDetail, tvShows } from './responses/tvShowResponse';

const server = setupServer(
  rest.get(`${process.env.REACT_APP_BASE_URL}/discover/${APIRoutes.movies}`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(movies));
  }),
  rest.get(`${process.env.REACT_APP_BASE_URL}/${APIRoutes.movies}/1`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(movieDetail.content));
  }),
  rest.get(`${process.env.REACT_APP_BASE_URL}/${APIRoutes.movies}/1/credits`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(movieDetail.credits));
  }),
  rest.get(`${process.env.REACT_APP_BASE_URL}/${APIRoutes.movies}/1/reviews`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(movieDetail.reviews));
  }),
  rest.get(`${process.env.REACT_APP_BASE_URL}/${APIRoutes.movies}/1/similar`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(movieDetail.similar));
  }),
  rest.get(`${process.env.REACT_APP_BASE_URL}/${APIRoutes.person}/1`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(personDetail));
  }),
  rest.get(`${process.env.REACT_APP_BASE_URL}/${APIRoutes.search}`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(search));
  }),
  rest.get(`${process.env.REACT_APP_BASE_URL}/discover/${APIRoutes.tvShows}`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(tvShows));
  }),
  rest.get(`${process.env.REACT_APP_BASE_URL}/${APIRoutes.tvShows}/1/season/1`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(seasonDetail));
  }),
  rest.get(`${process.env.REACT_APP_BASE_URL}/${APIRoutes.tvShows}/1`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(tvShowDetail.content));
  }),
  rest.get(`${process.env.REACT_APP_BASE_URL}/${APIRoutes.tvShows}/1/credits`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(tvShowDetail.credits));
  }),
  rest.get(`${process.env.REACT_APP_BASE_URL}/${APIRoutes.tvShows}/1/reviews`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(tvShowDetail.reviews));
  }),
  rest.get(`${process.env.REACT_APP_BASE_URL}/${APIRoutes.tvShows}/1/similar`, (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(tvShowDetail.similar));
  })
);

beforeAll(() => server.listen());
afterAll(() => server.close());
afterEach(() => server.resetHandlers());

export { server, rest };
