import { TDetailContent, TDetailCredits, TDetailReviews, TDetailSimilar, TMovie, TMoviesResponse } from '../../../types/responses/movies';

// --------- MOVIES
const movieArray: TMovie[] = [
  {
    adult: false,
    backdrop_path: '/70nxSw3mFBsGmtkvcs91PbjerwD.jpg',
    genre_ids: [878, 28, 12],
    id: 580489,
    original_language: 'en',
    original_title: 'Venom: Let There Be Carnage',
    overview:
      'After finding a host body in investigative reporter Eddie Brock, the alien symbiote must face a new enemy, Carnage, the alter ego of serial killer Cletus Kasady.',
    popularity: 24007.174,
    poster_path: '/rjkmN1dniUHVYAtwuV3Tji7FsDO.jpg',
    release_date: '2021-09-30',
    title: 'Venom: Let There Be Carnage',
    video: false,
    vote_average: 7.2,
    vote_count: 3451,
  },
  {
    adult: false,
    backdrop_path: '/cinER0ESG0eJ49kXlExM0MEWGxW.jpg',
    genre_ids: [28, 12, 14],
    id: 566525,
    original_language: 'en',
    original_title: 'Shang-Chi and the Legend of the Ten Rings',
    overview:
      'Shang-Chi must confront the past he thought he left behind when he is drawn into the web of the mysterious Ten Rings organization.',
    popularity: 5484.062,
    poster_path: '/1BIoJGKbXjdFDAqUEiA2VHqkK1Z.jpg',
    release_date: '2021-09-01',
    title: 'Shang-Chi and the Legend of the Ten Rings',
    video: false,
    vote_average: 7.9,
    vote_count: 3716,
  },
  {
    adult: false,
    backdrop_path: '/dK12GIdhGP6NPGFssK2Fh265jyr.jpg',
    genre_ids: [28, 35, 80, 53],
    id: 512195,
    original_language: 'en',
    original_title: 'Red Notice',
    overview:
      "An Interpol-issued Red Notice is a global alert to hunt and capture the world's most wanted. But when a daring heist brings together the FBI's top profiler and two rival criminals, there's no telling what will happen.",
    popularity: 5865.04,
    poster_path: '/wdE6ewaKZHr62bLqCn7A2DiGShm.jpg',
    release_date: '2021-11-04',
    title: 'Red Notice',
    video: false,
    vote_average: 6.9,
    vote_count: 1514,
  },
  {
    adult: false,
    backdrop_path: '/zBkHCpLmHjW2uVURs5uZkaVmgKR.jpg',
    genre_ids: [16, 35, 10751],
    id: 585245,
    original_language: 'en',
    original_title: 'Clifford the Big Red Dog',
    overview:
      'As Emily struggles to fit in at home and at school, she discovers a small red puppy who is destined to become her best friend. When Clifford magically undergoes one heck of a growth spurt, becomes a gigantic dog and attracts the attention of a genetics company, Emily and her Uncle Casey have to fight the forces of greed as they go on the run across New York City. Along the way, Clifford affects the lives of everyone around him and teaches Emily and her uncle the true meaning of acceptance and unconditional love.',
    popularity: 4095.129,
    poster_path: '/ygPTrycbMSFDc5zUpy4K5ZZtQSC.jpg',
    release_date: '2021-11-10',
    title: 'Clifford the Big Red Dog',
    video: false,
    vote_average: 7.7,
    vote_count: 281,
  },
  {
    adult: false,
    backdrop_path: '/r2GAjd4rNOHJh6i6Y0FntmYuPQW.jpg',
    genre_ids: [12, 28, 53],
    id: 370172,
    original_language: 'en',
    original_title: 'No Time to Die',
    overview:
      'Bond has left active service and is enjoying a tranquil life in Jamaica. His peace is short-lived when his old friend Felix Leiter from the CIA turns up asking for help. The mission to rescue a kidnapped scientist turns out to be far more treacherous than expected, leading Bond onto the trail of a mysterious villain armed with dangerous new technology.',
    popularity: 3466.359,
    poster_path: '/iUgygt3fscRoKWCV1d0C7FbM9TP.jpg',
    release_date: '2021-09-29',
    title: 'No Time to Die',
    video: false,
    vote_average: 7.6,
    vote_count: 2211,
  },
];

export const movies: TMoviesResponse = {
  page: 1,
  results: movieArray,
  total_pages: 1,
  total_results: 5,
};

// --------- MOVIE DETAIL

const detailContent: TDetailContent = {
  adult: false,
  backdrop_path: '/70nxSw3mFBsGmtkvcs91PbjerwD.jpg',
  belongs_to_collection: {
    id: 558216,
    name: 'Venom Collection',
    poster_path: '/670x9sf0Ru8y6ezBggmYudx61yB.jpg',
    backdrop_path: '/rhLspFB1B8ZCkWEHFYmc3NKagzq.jpg',
  },
  budget: 110000000,
  genres: [
    {
      id: 878,
      name: 'Science Fiction',
    },
    {
      id: 28,
      name: 'Action',
    },
  ],
  homepage: 'https://www.venom.movie',
  id: 1,
  imdb_id: 'tt7097896',
  original_language: 'en',
  original_title: 'Venom: Let There Be Carnage',
  overview:
    'After finding a host body in investigative reporter Eddie Brock, the alien symbiote must face a new enemy, Carnage, the alter ego of serial killer Cletus Kasady.',
  popularity: 24007.174,
  poster_path: '/rjkmN1dniUHVYAtwuV3Tji7FsDO.jpg',
  production_companies: [
    {
      id: 7505,
      logo_path: '/837VMM4wOkODc1idNxGT0KQJlej.png',
      name: 'Marvel Entertainment',
      origin_country: 'US',
    },
  ],
  production_countries: [
    {
      iso_3166_1: 'CN',
      name: 'China',
    },
  ],
  release_date: '2021-09-30',
  revenue: 454000000,
  runtime: 97,
  spoken_languages: [
    {
      english_name: 'English',
      iso_639_1: 'en',
      name: 'English',
    },
  ],
  status: 'Released',
  tagline: '',
  title: 'Venom: Let There Be Carnage',
  video: false,
  vote_average: 7.2,
  vote_count: 3506,
};

const detailCredits: TDetailCredits = {
  id: 1,
  cast: [
    {
      adult: false,
      gender: 2,
      id: 2524,
      known_for_department: 'Acting',
      name: 'Tom Hardy',
      original_name: 'Tom Hardy',
      popularity: 70.428,
      profile_path: '/sOi4UZxflV07E7QXUaJTHROGlPU.jpg',
      cast_id: 1,
      character: 'Eddie Brock / Venom',
      credit_id: '5c5b3ebfc3a3683cc6885550',
      order: 0,
    },
    {
      adult: false,
      gender: 2,
      id: 57755,
      known_for_department: 'Acting',
      name: 'Woody Harrelson',
      original_name: 'Woody Harrelson',
      popularity: 22.811,
      profile_path: '/igxYDQBbTEdAqaJxaW6ffqswmUU.jpg',
      cast_id: 7,
      character: 'Cletus Kasady / Carnage',
      credit_id: '5c86bc069251410d3616ff8e',
      order: 1,
    },
    {
      adult: false,
      gender: 1,
      id: 1812,
      known_for_department: 'Acting',
      name: 'Michelle Williams',
      original_name: 'Michelle Williams',
      popularity: 16.458,
      profile_path: '/sXTP6wlqIDz1tDGLU3DFbklSTpq.jpg',
      cast_id: 9,
      character: 'Anne Weying',
      credit_id: '5d4b75510021340013269639',
      order: 2,
    },
  ],
  crew: [
    {
      adult: false,
      gender: 2,
      id: 149,
      known_for_department: 'Camera',
      name: 'Robert Richardson',
      original_name: 'Robert Richardson',
      popularity: 1.4,
      profile_path: '/lmJy31oZtN5l2VAGpkrt3xEXsxL.jpg',
      credit_id: '5d59628f61bac40eca4a728e',
      department: 'Camera',
      job: 'Director of Photography',
    },
    {
      adult: false,
      gender: 2,
      id: 1333,
      known_for_department: 'Acting',
      name: 'Andy Serkis',
      original_name: 'Andy Serkis',
      popularity: 13.005,
      profile_path: '/2aJLwOQK50Lx7PvIHGW1GMytTOL.jpg',
      credit_id: '5e866a52a5743d00180fe64e',
      department: 'Directing',
      job: 'Director',
    },
  ],
};

const detailReviews: TDetailReviews = {
  id: 1,
  page: 1,
  results: [
    {
      author: 'garethmb',
      author_details: {
        name: '',
        username: 'garethmb',
        avatar_path: '/https://secure.gravatar.com/avatar/3593437cbd05cebe0a4ee753965a8ad1.jpg',
        rating: null,
      },
      content: 'Awesome movie!',
      created_at: '2021-09-30T13:48:10.279Z',
      id: '6155c01a8e2ba6008bdb3393',
      updated_at: '2021-09-30T13:48:10.279Z',
      url: 'https://www.themoviedb.org/review/6155c01a8e2ba6008bdb3393',
    },
    {
      author: 'Cal',
      author_details: {
        name: 'Cal',
        username: 'bycalvin',
        avatar_path: null,
        rating: 7.0,
      },
      content: 'Awesome! I loved it',
      created_at: '2021-10-16T00:40:23.907Z',
      id: '616a1f77924ce6008d09fb70',
      updated_at: '2021-10-25T21:29:12.310Z',
      url: 'https://www.themoviedb.org/review/616a1f77924ce6008d09fb70',
    },
  ],
  total_pages: 1,
  total_results: 1,
};

const detailSimilar: TDetailSimilar<TMovie> = {
    page: 1,
    results: movieArray,
    total_pages: 1,
    total_results: 5
}

export const movieDetail = {
  content: detailContent,
  credits: detailCredits,
  reviews: detailReviews,
  similar: detailSimilar,
};
