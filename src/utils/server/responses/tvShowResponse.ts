import { TDetailCredits, TDetailReviews, TDetailSimilar } from '../../../types/responses/movies';
import {
  TSeasonDetail,
  TTvShow,
  TTvShowDetailContent,
  TTvShowResponse,
  TTvShowsResponse,
} from '../../../types/responses/tvShows';

// --------- TV SHOWS

const tvShowArray = [
  {
    backdrop_path: '/87IhSzFxzvZIFmLOzfl4HJPe4p2.jpg',
    first_air_date: '2021-11-03',
    genre_ids: [10765, 9648, 18],
    id: 119955,
    name: 'Dr. Brain',
    origin_country: ['KR'],
    original_language: 'ko',
    original_name: 'Dr. 브레인',
    overview:
      'An emotional journey that follows a brain scientist who is obsessive about figuring out new technologies to access the consciousness and memories of the brain. His life goes sideways when his family falls victim to a mysterious accident, and  he uses his skills to access memories from his wife’s brain to piece together the mystery of what actually happened to his family and why.',
    popularity: 831.453,
    poster_path: '/3bYYLQAFWHsTtJyZb0vIReJJv07.jpg',
    vote_average: 8.2,
    vote_count: 18,
  },
  {
    backdrop_path: '/8lBlBItnehgOAwFt0ezxlXuWeIO.jpg',
    first_air_date: '2021-11-08',
    genre_ids: [10766, 18],
    id: 132375,
    name: 'Um Lugar ao Sol',
    origin_country: ['BR'],
    original_language: 'pt',
    original_name: 'Um Lugar ao Sol',
    overview: '',
    popularity: 830.144,
    poster_path: '/8OYUkmD4fFtbG4MT4uYvnA632Cg.jpg',
    vote_average: 10,
    vote_count: 1,
  },
  {
    backdrop_path: '/ktDJ21QQscbMNQfPpZBsNORxdDx.jpg',
    first_air_date: '2016-01-25',
    genre_ids: [80, 10765],
    id: 63174,
    name: 'Lucifer',
    origin_country: ['US'],
    original_language: 'en',
    original_name: 'Lucifer',
    overview:
      "Bored and unhappy as the Lord of Hell, Lucifer Morningstar abandoned his throne and retired to Los Angeles, where he has teamed up with LAPD detective Chloe Decker to take down criminals. But the longer he's away from the underworld, the greater the threat that the worst of humanity could escape.",
    popularity: 823.005,
    poster_path: '/ekZobS8isE6mA53RAiGDG93hBxL.jpg',
    vote_average: 8.5,
    vote_count: 11049,
  },
  {
    backdrop_path: '/eUMwG5vXg4ovEUvXLAFgrr4bQvp.jpg',
    first_air_date: '2010-10-31',
    genre_ids: [10759, 18, 10765],
    id: 1402,
    name: 'The Walking Dead',
    origin_country: ['US'],
    original_language: 'en',
    original_name: 'The Walking Dead',
    overview:
      "Sheriff's deputy Rick Grimes awakens from a coma to find a post-apocalyptic world dominated by flesh-eating zombies. He sets out to find his family and encounters many other survivors along the way.",
    popularity: 791.746,
    poster_path: '/w21lgYIi9GeUH5dO8l3B9ARZbCB.jpg',
    vote_average: 8.1,
    vote_count: 12078,
  },
  {
    backdrop_path: '/eD2U2RwxQosUDwvu19n46KvJEf9.jpg',
    first_air_date: '2021-09-13',
    genre_ids: [18, 35],
    id: 124549,
    name: 'Marry Me, Marry You',
    origin_country: ['PH'],
    original_language: 'tl',
    original_name: 'Marry Me, Marry You',
    overview:
      'It will center on a couple, portrayed by Gutierrez and Avelino, who navigates the traditional expectations attached to marriage — that it extends beyond one’s partner, and includes their friends and family, too.',
    popularity: 761.862,
    poster_path: '/7qZUC0AQmSi7pxNP6cH3swkb8Is.jpg',
    vote_average: 5,
    vote_count: 2,
  },
];

export const tvShows: TTvShowsResponse = {
  page: 1,
  results: tvShowArray,
  total_pages: 1,
  total_results: 5,
};

// --------- SEASON DETAIL

export const seasonDetail: TSeasonDetail = {
  _id: '5da6ba97a2423200127893a2',
  air_date: '2021-11-06',
  episodes: [
    {
      air_date: '2021-11-06',
      episode_number: 1,
      crew: [
        {
          job: 'Writer',
          department: 'Writing',
          credit_id: '6196d2c729733800920d3b0e',
          adult: false,
          gender: 2,
          id: 12905,
          known_for_department: 'Writing',
          name: 'Ash Brannon',
          original_name: 'Ash Brannon',
          popularity: 0.84,
          profile_path: '/meH6AhBnCyX4w0lBFvRQWDJmvjv.jpg',
        },
      ],
      guest_stars: [
        {
          character: 'Vander (voice)',
          credit_id: '61925f6a01757f002c9514b3',
          order: 2,
          adult: false,
          gender: 2,
          id: 136152,
          known_for_department: 'Acting',
          name: 'J. B. Blanc',
          original_name: 'J. B. Blanc',
          popularity: 3.428,
          profile_path: '/5c0FSFRTfB6Ejtu7Sa0jmhDShfL.jpg',
        },
      ],
      id: 1,
      name: 'Welcome to the Playground',
      overview:
        "Orphaned sisters Vi and Powder bring trouble to Zaun's underground streets in the wake of a heist in posh Piltover.",
      production_code: '',
      season_number: 1,
      still_path: '/g7dMkFJvJECDYoL0TRzGQCfF3xD.jpg',
      vote_average: 8.7,
      vote_count: 11,
    },
  ],
  name: 'Season 1',
  overview: '',
  id: 1,
  poster_path: '/xQ6GijOFnxTyUzqiwGpVxgfcgqI.jpg',
  season_number: 1,
};

// --------- TV SHOW DETAIL

const detailTvContent: TTvShowDetailContent = {
  backdrop_path: '/rkB4LyZHo1NHXFEDHl9vSD9r1lI.jpg',
  created_by: [
    {
      id: 2000007,
      credit_id: '6186be821c6aa7002ca2971e',
      name: 'Christian Linke',
      gender: 2,
      profile_path: '/yTYR4W1ORuSBoVPjnkGafNPKQDc.jpg',
    },
    {
      id: 3299121,
      credit_id: '6186be85d388ae0043fe3adb',
      name: 'Alex Yee',
      gender: 2,
      profile_path: null,
    },
  ],
  episode_run_time: [42],
  first_air_date: '2021-11-06',
  genres: [
    {
      id: 16,
      name: 'Animation',
    },
    {
      id: 10765,
      name: 'Sci-Fi & Fantasy',
    },
    {
      id: 10759,
      name: 'Action & Adventure',
    },
    {
      id: 18,
      name: 'Drama',
    },
  ],
  homepage: 'https://arcane.com/',
  id: 1,
  in_production: true,
  languages: ['en'],
  last_air_date: '2021-11-20',
  last_episode_to_air: {
    air_date: '2021-11-20',
    episode_number: 9,
    id: 3246870,
    name: 'The Monster You Created',
    overview:
      'Perilously close to war, the leaders of Piltover and Zaun reach an ultimatum. But a fateful standoff changes both cities forever.',
    production_code: '',
    season_number: 1,
    still_path: '/dbnGqpGWUjLcTVQxr5k1V3no8BB.jpg',
    vote_average: 8.0,
    vote_count: 5,
  },
  name: 'Arcane',
  next_episode_to_air: null,
  networks: [
    {
      name: 'Netflix',
      id: 213,
      logo_path: '/wwemzKWzjKYJFfCeiB57q3r4Bcm.png',
      origin_country: '',
    },
  ],
  number_of_episodes: 9,
  number_of_seasons: 2,
  origin_country: ['US'],
  original_language: 'en',
  original_name: 'Arcane',
  overview:
    'Amid the stark discord of twin cities Piltover and Zaun, two sisters fight on rival sides of a war between magic technologies and clashing convictions.',
  popularity: 1691.368,
  poster_path: '/fqldf2t8ztc9aiwn3k6mlX3tvRT.jpg',
  production_companies: [
    {
      id: 99496,
      logo_path: '/6WTCdsmIH6qR2zFVHlqjpIZhD5A.png',
      name: 'Fortiche Production',
      origin_country: 'FR',
    },
    {
      id: 124172,
      logo_path: '/sBlhznEktXKBqC87Bsfwpo1YbYR.png',
      name: 'Riot Games',
      origin_country: 'US',
    },
  ],
  production_countries: [
    {
      iso_3166_1: 'US',
      name: 'United States of America',
    },
  ],
  seasons: [
    {
      air_date: '2021-11-06',
      episode_count: 9,
      id: 134187,
      name: 'Season 1',
      overview: '',
      poster_path: '/xQ6GijOFnxTyUzqiwGpVxgfcgqI.jpg',
      season_number: 1,
    },
    {
      air_date: null,
      episode_count: 0,
      id: 220416,
      name: 'Season 2',
      overview: '',
      poster_path: null,
      season_number: 2,
    },
  ],
  spoken_languages: [
    {
      english_name: 'English',
      iso_639_1: 'en',
      name: 'English',
    },
  ],
  status: 'Returning Series',
  tagline: '',
  type: 'Scripted',
  vote_average: 9.2,
  vote_count: 905,
};

const detailTvCredits: TDetailCredits = {
  id: 1,
  cast: [
    {
      adult: false,
      gender: 1,
      id: 130640,
      known_for_department: 'Acting',
      name: 'Hailee Steinfeld',
      original_name: 'Hailee Steinfeld',
      popularity: 48.675,
      profile_path: '/dxSDWkiVaC6JYjrV3XRAZI7HOSS.jpg',
      character: 'Vi (voice)',
      credit_id: '614fdc8c397561008c5d43a0',
      order: 0,
    },
  ],
  crew: [
    {
      adult: false,
      gender: 0,
      id: 3299109,
      known_for_department: 'Production',
      name: 'Marc Merrill',
      original_name: 'Marc Merrill',
      popularity: 0.6,
      profile_path: null,
      credit_id: '6186670375f1ad008e413746',
      department: 'Production',
      job: 'Executive Producer',
    },
  ],
};

const detailTvReviews: TDetailReviews = {
  id: 94605,
  page: 1,
  results: [
    {
      author: 'Folio Swami',
      author_details: {
        name: 'Folio Swami',
        username: 'FolioSwami',
        avatar_path: '/mHc1I1YnZPvXuWhhB0CXWeVxDyk.png',
        rating: 4.0,
      },
      content:
        "The production team has worked real hard to make something worth the money they were given. The characters are well drawn. And the animation is okay. The effort is obvious seeing the catch lights that are on any character, regardless of lighting.\r\n\r\nThe story is rather boring, and uniform. Stealing is good. Capitalists are bad. Magic exists. It's all a mess. So far Netflix has released only three episodes, and the idea is how good it is to have people live on taxes. Don't worry little serf, your offspring will have a beautiful life thanks to the taxes you paid.",
      created_at: '2021-11-10T16:07:45.104Z',
      id: '618bee51ddd52d0026c3a184',
      updated_at: '2021-11-10T16:07:45.104Z',
      url: 'https://www.themoviedb.org/review/618bee51ddd52d0026c3a184',
    },
  ],
  total_pages: 1,
  total_results: 1,
};

const detailTvSimilar: TDetailSimilar<TTvShow> = {
  page: 1,
  results: tvShowArray,
  total_pages: 1,
  total_results: 5,
};

export const tvShowDetail: TTvShowResponse = {
  content: detailTvContent,
  credits: detailTvCredits,
  reviews: detailTvReviews,
  similar: detailTvSimilar,
};
