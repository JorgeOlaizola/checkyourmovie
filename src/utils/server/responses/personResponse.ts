import { TPersonDetail } from "../../../types/responses/person";

export const personDetail: TPersonDetail = {
  adult: false,
  also_known_as: [
    'George Walton Lucas Jr. ',
    '乔治·卢卡斯',
    'Джордж Лукас',
    'ジョージ・ルーカス',
    'จอร์จ ลูคัส',
    '조지 루카스',
    'جورج لوكاس',
  ],
  biography:
    'George Walton Lucas Jr. (born May 14, 1944) is an American filmmaker and entrepreneur.',
  birthday: '1944-05-14',
  deathday: null,
  gender: 2,
  homepage: null,
  id: 1,
  imdb_id: 'nm0000184',
  known_for_department: 'Directing',
  name: 'George Lucas',
  place_of_birth: 'Modesto, California, USA',
  popularity: 5.267,
  profile_path: '/WCSZzWdtPmdRxH9LUCVi2JPCSJ.jpg',
};
