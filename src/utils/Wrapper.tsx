import React, { FunctionComponent, ReactElement, ReactNode } from 'react';
import { BrowserRouter, useLocation } from 'react-router-dom';
import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import { store } from '../state/store';

type PropT = {
  children: ReactNode;
};

function LocationDisplay() {
  const location = useLocation();
  return <div data-testid='location-display'>{location.pathname}</div>;
}

function Wrapper({ children }: PropT) {
  return (
    <BrowserRouter>
      <Provider store={store}>
        {children}
        <LocationDisplay />
      </Provider>
    </BrowserRouter>
  );
}

function renderWithRouter(UI: ReactElement, route = '/') {
  window.history.pushState({}, 'Test Page', route);
  return render(UI, { wrapper: Wrapper as FunctionComponent });
}

export default renderWithRouter;
