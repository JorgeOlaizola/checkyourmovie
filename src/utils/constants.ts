export const LOGO =
  'https://res.cloudinary.com/jorgeleandroolaizola/image/upload/v1638055519/4ebfab24058243c29ce08fd4c6d06d2f_1_vsqyq4.png';
export const MARGIN_PAGE = 'mt-20';
export const IMAGES_URL = 'https://image.tmdb.org/t/p/w500';
export const IMAGES_URL_ORIGINAL = 'https://image.tmdb.org/t/p/original';
export const DEFAULT_IMAGE = 'https://thesocietypages.org/socimages/files/2009/05/nopic_192.gif';
